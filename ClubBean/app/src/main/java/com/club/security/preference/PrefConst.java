package com.club.security.preference;

/**
 * Created by HGS on 12/11/2015.
 */
public class PrefConst {


    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_USERNAME = "username";

    public static final String PREFKEY_INCREASED = "increased_id";

}
