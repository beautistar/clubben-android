package com.club.security.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.club.security.ClubApplication;
import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.club.security.commons.Constants;
import com.club.security.commons.ReqConst;
import com.club.security.model.IncidentEntity;
import com.club.security.utils.RadiusImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@SuppressLint("ValidFragment")
public class DisputeDetailsFragment extends Fragment implements View.OnClickListener{

    MainActivity _activity;
    View view;
    RadiusImageView imv_incident_photo;
    TextView txv_description, txv_submit;
    EditText edt_comment;

    IncidentEntity _error = new IncidentEntity();

    public DisputeDetailsFragment(MainActivity activity, IncidentEntity entity) {
        // Required empty public constructor
        this._activity = activity;
        _error = entity;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dispute_details, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        imv_incident_photo = (RadiusImageView) view.findViewById(R.id.imv_photo);
        txv_description = (TextView)view.findViewById(R.id.txv_description);

        txv_submit = (TextView)view.findViewById(R.id.txv_submit);
        txv_submit.setOnClickListener(this);

        edt_comment = (EditText)view.findViewById(R.id.edt_comment);

        LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.lyt_container);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_comment.getWindowToken(),0);

                return false;
            }
        });

        showDetails();
    }

    private void showDetails() {

        if (_error.get_incident_picture().length() > 0)
            Glide.with(_activity).load(_error.get_incident_picture()).placeholder(R.drawable.bg_non_profile).into(imv_incident_photo);

        txv_description.setText(_error.get_incident_description());
    }


    private void subMitDispute(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SUBMIT_DISPUTE;

        Log.d("url==============>", url);

        _activity.showProgress();

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSubMitDispute(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {


                    params.put("incident_id", String.valueOf(_error.get_id()));
                    params.put("comment", edt_comment.getText().toString());

                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseSubMitDispute(String json) {

        _activity.closeProgress();

        Log.d("Response=SubMitDisput=>", json );

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code ==  ReqConst.CODE_SUCCESS){

                _activity.showToast("Successfully");
                _activity.gotoLandingPageFragment();

            } else if (result_code == ReqConst.CODE_WAIT){

                _activity.closeProgress();
                _activity.showAlertDialog("Already submitted\nWaiting approve by admin...");
            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(String.valueOf(result_code));
            }
        } catch (JSONException e) {

            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_submit:

                InputMethodManager imm = (InputMethodManager)_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txv_submit.getWindowToken(),0);

                subMitDispute();
                break;
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }


}
