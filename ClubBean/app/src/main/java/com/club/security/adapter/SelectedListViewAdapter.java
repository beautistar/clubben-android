package com.club.security.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.club.security.commons.Commons;
import com.club.security.fragment.PatronsPresentFragment;
import com.club.security.model.PatronEntity;

import java.util.ArrayList;

/**
 * Created by HugeRain on 4/29/2017.
 */

public class SelectedListViewAdapter extends BaseAdapter {

    MainActivity _activity;
    PatronsPresentFragment _fragment;
    ArrayList<PatronEntity> _users = new ArrayList<>();
    ArrayList<PatronEntity> _allUser = new ArrayList<>();

    public SelectedListViewAdapter(MainActivity activity, PatronsPresentFragment fragment){

        this._activity = activity;
        this._fragment = fragment;
    }

    public void addItem(PatronEntity user){

        //_allUser.add(user);
        _users.add(user);
        Commons.ALL_PATRON = _users;
        notifyDataSetChanged();

    }

    public void removeItem(PatronEntity remove){

        _users.remove(remove);
        Commons.ALL_PATRON = _users;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return _users.size();
    }

    @Override
    public Object getItem(int position) {
        return _users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        UserHolder holder;

        if (convertView == null){

            holder = new UserHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_selected_list, parent, false);

            holder.imv_photo = (ImageView)convertView.findViewById(R.id.imv_photo);
            holder.txv_userName = (TextView)convertView.findViewById(R.id.txv_user);

            convertView.setTag(holder);

        } else {

            holder = (UserHolder)convertView.getTag();
        }

        PatronEntity user = _users.get(position);

        Glide.with(_activity).load(user.get_photo_url()).placeholder(R.drawable.bg_non_profile).into(holder.imv_photo);
        //holder.txv_userName.setText(user.get_scan_id());
        holder.txv_userName.setText(user.get_daq_id());

   /*     convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.gotoIncidentDescriptionActivity();
            }
        });
*/
        return convertView;
    }

    public class UserHolder{

        ImageView imv_photo;
        TextView txv_userName;
    }
}
