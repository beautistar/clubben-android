package com.club.security.commons;

import android.os.Handler;

import com.club.security.base.CommonActivity;
import com.club.security.model.IncidentEntity;
import com.club.security.model.PatronEntity;
import com.club.security.model.UserEntity;

import java.util.ArrayList;


/**
 * Created by HGS on 12/11/2015.
 */
public class Commons {

    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;



    public static Handler g_handler = null;
    public static String g_appVersion = "1.0";

    public static UserEntity g_user = null;
    //public static PatronEntity g_newUser = null;
    public static PatronEntity g_patron = null;

    public static ArrayList<PatronEntity> ALL_PATRON = new ArrayList<>();

    public static IncidentEntity g_error = null;

    public static  int PATRON_ID = 0;
    public static String PHOTO_PATH = "";
    public static String INCIDENTID = "";


    public static CommonActivity g_currentActivity = null;


    public static int addrToIdx(String addr) {
        int pos = addr.indexOf("@");
        return Integer.valueOf(addr.substring(0, pos)).intValue();
    }

    public static String fileExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf(".") == -1) {
            return url;
        } else {
            String ext = url.substring(url.lastIndexOf(".") );
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

    public static String fileNameWithExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf("/") == -1) {
            return url;
        } else {
            String name = url.substring(url.lastIndexOf("/")  + 1);
            return name;
        }
    }

    public static String fileNameWithoutExtFromUrl(String url) {

        String fullname = fileNameWithExtFromUrl(url);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

    public static String fileNameWithExtFromPath(String path) {

        if (path.lastIndexOf("/") > -1)
            return path.substring(path.lastIndexOf("/") + 1);

        return path;
    }

    public static String fileNameWithoutExtFromPath(String path) {

        String fullname = fileNameWithExtFromPath(path);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

}
