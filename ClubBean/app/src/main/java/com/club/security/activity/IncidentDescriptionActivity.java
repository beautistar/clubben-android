package com.club.security.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.club.security.ClubApplication;
import com.club.security.R;
import com.club.security.base.CommonActivity;
import com.club.security.commons.Commons;
import com.club.security.commons.Constants;
import com.club.security.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class IncidentDescriptionActivity extends CommonActivity implements View.OnClickListener {

    ImageView imv_back;
    EditText edt_description;
    TextView txv_violent, txv_intoxicated, txv_sexual, txv_soliciting, txv_uncooperative, txv_armed, txv_stealing , txv_submit;

    String _iin = "", _daq = "", _dba = "", _dbb = "";
    int _patron_id = 0;
    String _photo_path = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_description);

        edt_description = (EditText)findViewById(R.id.edt_description);

        Log.d("STATUS ++ ", String.valueOf(Constants._from));

        if (Constants._from == Constants.FROM_SCAN) {

            _iin = getIntent().getStringExtra(Constants.KEY_IIN);
            _daq = getIntent().getStringExtra(Constants.KEY_DAQ);
            _dba = getIntent().getStringExtra(Constants.KEY_DBA);
            _dbb = getIntent().getStringExtra(Constants.KEY_DBB);

            Log.d("IIN form scan ====", _iin);

        } else if (Constants._from == Constants.FROM_SUBMISSION) {

            Log.d("IIN from back ====", String.valueOf(Commons.PATRON_ID));
            _patron_id = Commons.PATRON_ID;
            _photo_path = Commons.PHOTO_PATH;

            edt_description.setText(Constants.KEEP_DESCRIPTION);

        } else {
            edt_description.setText(Constants.DESCRIPTION);
        }

        loadLayout();
    }

    private void loadLayout() {



        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.DESCRIPTION = "";
                Constants.KEEP_DESCRIPTION = "";
                Constants.CREATE = 0;
                gotoReportIncident();
            }
        });

        Constants.DESCRIPTION = edt_description.getText().toString();
        edt_description.setFocusable(true);

        txv_violent = (TextView)findViewById(R.id.txv_violent);
        txv_violent.setOnClickListener(this);

        txv_intoxicated = (TextView)findViewById(R.id.txv_intoxicated);
        txv_intoxicated.setOnClickListener(this);

        txv_sexual = (TextView)findViewById(R.id.txv_sexual);
        txv_sexual.setOnClickListener(this);

        txv_soliciting = (TextView)findViewById(R.id.txv_soliciting);
        txv_soliciting.setOnClickListener(this);

        txv_uncooperative = (TextView)findViewById(R.id.txv_uncooperative);
        txv_uncooperative.setOnClickListener(this);

        txv_armed = (TextView)findViewById(R.id.txv_armed);
        txv_armed.setOnClickListener(this);

        txv_stealing = (TextView)findViewById(R.id.txv_stealing);
        txv_stealing.setOnClickListener(this);

        txv_submit = (TextView)findViewById(R.id.txv_submit);
        txv_submit.setOnClickListener(this);

        edt_description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Constants.DESCRIPTION = edt_description.getText().toString() ;
                if (Constants._from != Constants.FROM_SUBMISSION)Constants.KEEP_DESCRIPTION = edt_description.getText().toString();
            }
        });

        if (/*Constants.CREATE == 3 && */Constants._from == Constants.FROM_SCAN)
            getPatronByScan();
    }

    private void getPatronByScan(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_PATRON_BYSCAN;

        try {

            String IIN = _iin.toString().trim().replace(" ", "%20");
            IIN = URLEncoder.encode(IIN, "utf-8");

            String DAQ = _daq.toString().trim().replace(" ", "%20");
            DAQ = URLEncoder.encode(DAQ, "utf-8");

            String DBA = _dba.toString().trim().replace(" ", "%20");
            DBA = URLEncoder.encode(DBA, "utf-8");

            String DBB = _dbb.toString().trim().replace(" ", "%20");
            DBB = URLEncoder.encode(DBB, "utf-8");

            String params = String.format("/%s/%s/%s/%s", IIN, DAQ, DBA, DBB );
            url += params ;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parsePatronByScan(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parsePatronByScan(String json) {

        closeProgress();

        try {
            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _patron_id = response.getInt(ReqConst.RES_PATRON_ID);
                _photo_path = response.getString(ReqConst.RES_PHOTO_URL);

                Commons.PATRON_ID = _patron_id;
                Commons.PHOTO_PATH = _photo_path;

            } else if (result_code == ReqConst.CODE_UNKNOWN){

                showAlertDialog("This patron doesn't exist");
                closeProgress();

            }
        } catch (JSONException e) {
            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void gotoReportIncident() {

        startActivity(new Intent(this, MainActivity.class));
    }

    public boolean checkValid(){

        if (edt_description.getText().toString().length() == 0){

            showAlertDialog("Please input the description");
            return false;

        } else if (Commons.PATRON_ID == 0 && Commons.ALL_PATRON.size() == 0){

            showAlertDialog("Please try again");
            return false;
        }
        return true;
    }

    private void addDescription(String str){

        if (edt_description.getText().toString().length() > 0){

            edt_description.setText(Constants.DESCRIPTION + ", " + str);
        } else {
            if (Constants.DESCRIPTION.toString().length() > 0)edt_description.setText(Constants.DESCRIPTION + str);

            else {

                String string = str.substring(0,1).toUpperCase() + str.substring(1).toLowerCase();
                Constants.DESCRIPTION = string;
                edt_description.setText(Constants.DESCRIPTION);

            };
        }

        Constants.DESCRIPTION = edt_description.getText().toString();

        if (Constants._from != Constants.FROM_SUBMISSION)
            Constants.KEEP_DESCRIPTION = edt_description.getText().toString();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_violent:

                addDescription("violent");
                break;

            case R.id.txv_intoxicated:

                addDescription("intoxicated");
                break;

            case R.id.txv_sexual:
                addDescription("sexual");
                break;

            case R.id.txv_soliciting:
                addDescription("soliciting");
                break;

            case R.id.txv_uncooperative:
                addDescription("uncooperative");
                break;

            case R.id.txv_armed:
                addDescription("armed");
                break;

            case R.id.txv_stealing:
                addDescription("stealing");
                break;

            case R.id.txv_submit:

                Constants._from = 0;

                InputMethodManager imm6 = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm6.hideSoftInputFromWindow(txv_submit.getWindowToken(),0);

                if (checkValid()){

                    Intent intent = new Intent(this, SubmissionActivity.class);
                    intent.putExtra(Constants.KEY_PATRON_ID, _patron_id);
                    intent.putExtra(Constants.KEY_PHOTO_URL, "1,2");
                    intent.putExtra(Constants.KEY_PHOTO_URL, _photo_path);
                    intent.putExtra(Constants.KEY_DESCRIPTION, edt_description.getText().toString());
                    startActivity(intent);
                    finish();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        gotoReportIncident();
    }
}
