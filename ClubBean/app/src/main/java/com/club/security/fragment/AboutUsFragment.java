package com.club.security.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.microblink.activity.Pdf417ScanActivity;

import java.io.InputStream;

public class AboutUsFragment extends Fragment {

    MainActivity _activity;
    View view;

    TextView txv_about;
    String result;

    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_about_us, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        txv_about = (TextView)view.findViewById(R.id.txv_about);

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.about_us);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
        } catch (Exception e) {
            result = "Error : can't show file."  ;
        }

        txv_about.setText(result.toString());

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}
