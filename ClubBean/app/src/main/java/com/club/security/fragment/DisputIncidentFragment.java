package com.club.security.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.club.security.ClubApplication;
import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.club.security.adapter.ErrorListViewAdapter;
import com.club.security.adapter.EvaluationListViewAdapter;
import com.club.security.commons.Constants;
import com.club.security.commons.ReqConst;
import com.club.security.model.IncidentEntity;
import com.club.security.model.PatronEntity;
import com.club.security.model.StateEntity;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


@SuppressLint("ValidFragment")
public class DisputIncidentFragment extends Fragment implements View.OnClickListener{


    MainActivity _activity;
    View view;

    ListView lst_error;
    ErrorListViewAdapter _adapter;

    EditText edt_swip_id;

    TextView txv_submit;
    TextView txv_entry;


    String _iin = "", _daq = "", _dba = "3", _dbb = "4";
    int _id = 0;

    String[] state_name = {"Alabama", "Alaska","Arizona", "Arkansas", "British Columbia", "California", "Coahuila", "Colorado","Connecticut","Delaware",
            "District of Columbia", "Florida","Georgia", "Guam", "Hawaii", "Hidalgo", "Idaho", "Illinois","Indiana","Iowa",
            "Kansas", "Kentucky","Louisiana", "Maine", "Manitoba", "Maryland", "Massachusetts", "Michigan","Minnesota","Mississippi",
            "Missouri", "Montana","Nebraska", "Nevada", "New Brunswick", "New Hampshire", "New Jersey", "New Mexico", "New York","Newfoundland",
            "North Carolina", "North Dakota","Nova Scotia", "Ohio", "Oklahoma", "Ontario", "Oregon", "Pennsylvania", "Prince Edward Island","Rhode Island",
            "Saskatchewan", "South Carolina","South Dakota", "State Dept (USA)", "Tennessee", "Texas", "US Virgin Islands", "Utah", "Vermont","Virginia",
            "Washington", "West Virginia","Wisconsin", "Wyoming"};

    int[] state_iin = {636033, 636059, 636026, 636021, 636028, 636014 , 636056, 636020, 636006, 636011,
            636043, 636010, 636055, 636019, 636047, 636057, 636050, 636035, 636037, 636018,
            636022, 636046, 636007, 636041, 636048, 636003, 636002, 636032, 636038, 636051,
            636030, 636008, 636054, 636049, 636017, 636039, 636036, 636009, 636001, 636016,
            636004, 636034, 636013, 636023, 636058, 636012, 636029, 636025, 604425, 636052,
            636044, 636005, 636042, 636027, 636053, 636015, 636062, 636040, 636024, 636000,
            636045, 636061, 636031, 636060};

    PatronEntity _evaluation = new PatronEntity();
    ArrayList<IncidentEntity> _incidents = new ArrayList<>();

    MaterialSpinner spinner;
    String[] strings = null;

    ArrayList<StateEntity> _states = new ArrayList<>();

    public DisputIncidentFragment(MainActivity activity) {
        // Required empty public constructor

        _activity = activity;
    }

    public DisputIncidentFragment(MainActivity activity, String IIN, String DAQ, String DBA, String DBB ) {
        // Required empty public constructor

        _activity = activity;
        this._iin = IIN;
        this._daq = DAQ;
        this._dba = DBA;
        this._dbb = DBB;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_disput_incident, container, false);


        for (int i = 0; i < 64; i++){

            StateEntity _state = new StateEntity();

            _state.set_id(i);
            _state.set_name(state_name[i]);
            _state.set_iin(state_iin[i]);

            _states.add(_state);
        }

        loadLayout();
        return view;
    }

    private void loadLayout() {

        lst_error = (ListView)view.findViewById(R.id.lst_error);

        edt_swip_id = (EditText)view.findViewById(R.id.edt_swipe_id);

        txv_entry = (TextView)view.findViewById(R.id.txv_entry);
        txv_entry.setOnClickListener(this);

        txv_submit = (TextView)view.findViewById(R.id.txv_submit);
        txv_submit.setOnClickListener(this);

        // container
        LinearLayout lytContainer = (LinearLayout) view.findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) _activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_swip_id.getWindowToken(), 0);
                return false;
            }
        });


        if (_iin.length() > 1 && _daq.length() > 1 && _dba.length() > 1 && _dbb.length() > 1){

            edt_swip_id.setText(_daq);
        }

        spinner = (MaterialSpinner)view.findViewById(R.id.spinner_incident);

        spinner.setItems(state_name);
        final String[] producttype = {state_name[0]};

        _iin = String.valueOf(state_iin[0]);

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

                producttype[0] = item;

                _iin = String.valueOf(state_iin[position]);

            }
        });
    }

    private void getDisputeIncidentList() {

        if (edt_swip_id.getText().toString().length() == 0) {
            showAlertDialog_wait("Please input the IIN ID.");
            return;
        }

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_DISPUTE_INCIDENT;

        try {

            String IIN = _iin.toString().trim().replace(" ", "%20");
            IIN = URLEncoder.encode(IIN, "utf-8");

            String DAQ = edt_swip_id.getText().toString().trim().replace(" ", "%20");
            DAQ = URLEncoder.encode(DAQ, "utf-8");

            String DBA = _dba.toString().trim().replace(" ", "%20");
            DBA = URLEncoder.encode(DBA, "utf-8");

            String DBB = _dbb.toString().trim().replace(" ", "%20");
            DBB = URLEncoder.encode(DBB, "utf-8");

            String params = String.format("/%s/%s/%s/%s", IIN, DAQ, DBA, DBB );
            url += params ;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        _activity.showProgress();

        Log.d("URL==DIS==>", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseEvaluation(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseEvaluation(String json){

        _activity.closeProgress();

        Log.d("D+++Res==>", json);

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                PatronEntity patron = new PatronEntity();

                patron.set_id(response.getInt(ReqConst.RES_PATRON_ID));
                patron.set_total_visit_count(response.getInt(ReqConst.RES_TOTAL_VISIT_CNT));
                patron.set_photo_url(response.getString(ReqConst.RES_PHOTO_URL));
                patron.set_total_incident_count(response.getInt(ReqConst.RES_TOTAL_INCIDENT_CNT));

                _id = patron.get_id();

                JSONArray incident_array = response.getJSONArray(ReqConst.RES_INCIDENT_LIST);
                _incidents = new ArrayList<>();

                for (int i = 0; i < incident_array.length(); i++){

                    IncidentEntity incident = new IncidentEntity();
                    JSONObject json_incident = (JSONObject)incident_array.get(i);

                    incident.set_id(json_incident.getInt(ReqConst.RES_INCIDENT_ID));
                    incident.set_incident_description(json_incident.getString(ReqConst.RES_INCIDENT_DES));
                    incident.set_incident_picture(json_incident.getString(ReqConst.RES_INCIDENT_PICTURE));
                    incident.set_incident_type(json_incident.getString(ReqConst.RES_INCIDENT_TYPE));
                    incident.set_incident_date(json_incident.getString(ReqConst.RES_INCIDENT_DATE));
                    incident.set_incident_dispute_status(json_incident.getInt(ReqConst.RES_INCIDENT_DISPUT_STATUS));

                    _incidents.add(incident);
                }

                patron.set_incident_list(_incidents);

                _evaluation = patron;

                _adapter = new ErrorListViewAdapter(_activity,  _incidents);
                lst_error.setAdapter(_adapter);

            } else if (result_code == ReqConst.CODE_UNKNOWN){

                _activity.closeProgress();
                _activity.showAlertDialog("Unregistered ID");
            }

        } catch (JSONException e) {
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void subMitDispute(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SUBMIT_DISPUTE;

        String params = String.format("/%d", _id);

        url += params;
        Log.d("url==============>", url);

        _activity.showProgress();

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSubMitDispute(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseSubMitDispute(String json) {

        _activity.closeProgress();

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code ==  ReqConst.CODE_SUCCESS){

               /* _activity.showToast("Successfully");
                _activity.gotoLandingPageFragment();*/

            } else if (result_code == ReqConst.CODE_WAIT){

                showAlertDialog_wait("You had submit already.\nPlease wait...");

            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(String.valueOf(result_code));
            }
        } catch (JSONException e) {

            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }

    public void showAlertDialog_wait(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(_activity).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _activity.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        _activity.gotoLandingPageFragment();
                    }
                });
        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    private boolean checkId(){

        if (edt_swip_id.getText().toString().length() == 0){

            _activity.showAlertDialog("Please input the id");
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_submit:
                //subMitDispute();
                if (checkId())
                getDisputeIncidentList();

                break;

            case R.id.txv_entry:
                Constants.CURRENT_PAGE = 3;
                _activity.scanPDF417();
                break;
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        Constants.CURRENT_PAGE = 0;
    }
}
