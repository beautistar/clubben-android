package com.club.security.model;

import java.io.Serializable;

/**
 * Created by HugeRain on 5/6/2017.
 */

public class UserEntity implements Serializable {

    int _idx = 0;
    String _user_name = "";
    String _email = "";
    int _permission = 0; //2: security 3: admission:
    int _club_id = 0;
    String _club_name = "";

    public int get_idx() {
        return _idx;
    }

    public void set_idx(int _idx) {
        this._idx = _idx;
    }

    public String get_user_name() {
        return _user_name;
    }

    public void set_user_name(String _user_name) {
        this._user_name = _user_name;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public int get_permission() {
        return _permission;
    }

    public void set_permission(int _permission) {
        this._permission = _permission;
    }

    public int get_club_id() {
        return _club_id;
    }

    public void set_club_id(int _club_id) {
        this._club_id = _club_id;
    }

    public String get_club_name() {
        return _club_name;
    }

    public void set_club_name(String _club_name) {
        this._club_name = _club_name;
    }
}
