package com.club.security.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.club.security.ClubApplication;
import com.club.security.R;
import com.club.security.adapter.EvaluationListViewAdapter;
import com.club.security.base.CommonActivity;
import com.club.security.commons.Commons;
import com.club.security.commons.Constants;
import com.club.security.commons.ReqConst;
import com.club.security.model.IncidentEntity;
import com.club.security.model.PatronEntity;
import com.club.security.utils.BitmapUtils;
import com.club.security.utils.MultiPartRequest;
import com.club.security.utils.RadiusImageView;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EvaluationActivity extends CommonActivity implements View.OnClickListener {

    ImageView imv_back;
    RadiusImageView imv_current_photo;
    TextView txv_visit_num, txv_event_num, txv_accept, txv_reject;

    private Uri _imageCaptureUri;
    String _photoPath = "";

    ExpandableHeightListView lst_view;
    EvaluationListViewAdapter _adapter;

    String _iin = "", _daq = "", _dba = "", _dbb = "";
    PatronEntity _evaluation = new PatronEntity();
    ArrayList<IncidentEntity> _incidents = new ArrayList<>();
    int _id = 0;
    float grade = 0 ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation);

        _iin = getIntent().getStringExtra(Constants.KEY_IIN);
        _daq = getIntent().getStringExtra(Constants.KEY_DAQ);
        _dba = getIntent().getStringExtra(Constants.KEY_DBA);
        _dbb = getIntent().getStringExtra(Constants.KEY_DBB);

        /*_iin = "aaa"; _daq = "bbb"; _dba = "ccc"; _dbb = "ddd";*/

        loadLayout();
    }

    private void loadLayout() {

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoMainActivity();
            }
        });

        imv_current_photo = (RadiusImageView) findViewById(R.id.imv_current_photo);
        imv_current_photo.setOnClickListener(this);

      /*  imv_event_photo = (ImageView)findViewById(R.id.imv_event_photo);
        imv_event_photo.setOnClickListener(this);*/

        /*imv_delete = (ImageView)findViewById(R.id.imv_delete);
        imv_delete.setOnClickListener(this);*/

        txv_visit_num = (TextView)findViewById(R.id.txv_visit_num);
        txv_event_num = (TextView)findViewById(R.id.txv_event_num);

        txv_accept = (TextView)findViewById(R.id.txv_accept);
        txv_accept.setOnClickListener(this);

        txv_reject = (TextView)findViewById(R.id.txv_reject);
        txv_reject.setOnClickListener(this);

        lst_view = (ExpandableHeightListView) findViewById(R.id.lst_item);
        lst_view.setExpanded(true);


        getEvaluation();

    }

    private void getEvaluation(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_EVALUATION;

        try {

            String IIN = _iin.toString().trim().replace(" ", "%20");
            IIN = URLEncoder.encode(IIN, "utf-8");

            String DAQ = _daq.toString().trim().replace(" ", "%20");
            DAQ = URLEncoder.encode(DAQ, "utf-8");

            String DBA = _dba.toString().trim().replace(" ", "%20");
            DBA = URLEncoder.encode(DBA, "utf-8");

            String DBB = _dbb.toString().trim().replace(" ", "%20");
            DBB = URLEncoder.encode(DBB, "utf-8");

            String params = String.format("/%d/%d/%s/%s/%s/%s", Commons.g_user.get_idx(), Commons.g_user.get_club_id(), IIN, DAQ, DBA, DBB );
            url += params ;
            Log.d("getEvelation url : ", url);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseEvaluation(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parseEvaluation(String json){

        closeProgress();

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                PatronEntity patron = new PatronEntity();

                patron.set_id(response.getInt(ReqConst.RES_PATRON_ID));
                patron.set_total_visit_count(response.getInt(ReqConst.RES_TOTAL_VISIT_CNT));
                patron.set_photo_url(response.getString(ReqConst.RES_PHOTO_URL));
                patron.set_total_incident_count(response.getInt(ReqConst.RES_TOTAL_INCIDENT_CNT));

                _id = patron.get_id();


                JSONArray incident_array = response.getJSONArray(ReqConst.RES_INCIDENT_LIST);

                for (int i = 0; i < incident_array.length(); i++){

                    IncidentEntity incident = new IncidentEntity();
                    JSONObject json_incident = (JSONObject)incident_array.get(i);

                    incident.set_id(json_incident.getInt(ReqConst.RES_INCIDENT_ID));
                    incident.set_incident_description(json_incident.getString(ReqConst.RES_INCIDENT_DES));
                    incident.set_incident_picture(json_incident.getString(ReqConst.RES_INCIDENT_PICTURE));
                    incident.set_incident_type(json_incident.getString(ReqConst.RES_INCIDENT_TYPE));
                    incident.set_incident_date(json_incident.getString(ReqConst.RES_INCIDENT_DATE));

                    _incidents.add(incident);
                }

                patron.set_incident_list(_incidents);

                _evaluation = patron;

                _adapter = new EvaluationListViewAdapter(this, _incidents);
                lst_view.setAdapter(_adapter);

                showEvaluation();

            } else {

                closeProgress();
                showAlertDialog(String.valueOf(result_code));
            }

        } catch (JSONException e) {
            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void showEvaluation() {

        if (_evaluation.get_photo_url().length() > 0){

            Glide.with(this).load(_evaluation.get_photo_url()).placeholder(R.drawable.bg_non_profile).into(imv_current_photo);
        }

        TextView txv_grade = (TextView)findViewById(R.id.txv_grade);


        if (_evaluation.get_total_visit_count() == 0){

            grade = 1;

        } else {

            grade = 100*(_evaluation.get_total_incident_count())/(_evaluation.get_total_visit_count()) ;
        }

        //95% or higher A, 90% or higher B, 85% or higher C less than 80% D
        
        if (grade > 0 && grade < 5){

            txv_grade.setText("A");
            txv_grade.setBackgroundResource(R.drawable.grade_a_circle);

        } else if (grade >= 5 && grade < 10){

            txv_grade.setText("B");
            txv_grade.setBackgroundResource(R.drawable.grade_b_circle);

        } else if (grade >= 10 && grade < 15){

            txv_grade.setText("C");
            txv_grade.setBackgroundResource(R.drawable.grade_c_circle);


        } else if (grade >= 15){

            txv_grade.setText("D");
            txv_grade.setBackgroundResource(R.drawable.grade_d_circle);
        }

        txv_visit_num.setText(String.valueOf(_evaluation.get_total_visit_count()));
        txv_event_num.setText(String.valueOf(_evaluation.get_total_incident_count()));
    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
//            fileTemp = ImageUtils.getOutputMediaFile();
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            _imageCaptureUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//            if (fileTemp != null) {
//            fileUri = Uri.fromFile(fileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
//            } else {
//                Toast.makeText(this, getString(R.string.error_create_image_file), Toast.LENGTH_LONG).show();
//            }
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
        }

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imv_current_photo.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void acceptPatronWithPhoto(){

        //if no profile photo
        if (_photoPath.length() == 0 /*&& _evaluation.get_photo_url().length() == 0*/){

            showAlertDialog("Please take photo");
            return;
        }

        try {

            showProgress();
            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();
            params.put("id", String.valueOf(_id));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_ACCEPT_PATRON_PHOTO ;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (isFinishing()){

                        closeProgress();
                        showAlertDialog(getString(R.string.photo_upload_fail));
                    }
                }

            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {

                    ParseUploadImageResponse(json);

                }

            },file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT ,0 ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            ClubApplication.getInstance().addToRequestQueue(reqMultiPart,url);

        } catch (Exception e){

            e.printStackTrace();
            closeProgress();
            showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    private void ParseUploadImageResponse(String json) {

        closeProgress();

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                gotoMainActivity();
                showToast("Success");

            }else {
                showAlertDialog(String.valueOf(result_code));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            showAlertDialog(getString(R.string.photo_upload_fail));
        }

    }

    public void gotoMainActivity(){

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void gotoPreview(String url){

        Intent intent =  new Intent(this, ImagePreviewActivity.class);
        intent.putExtra(Constants.KEY_IMAGEPATH, url);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_current_photo:
                selectPhoto();
                break;


            case R.id.txv_accept:
                acceptPatronWithPhoto();
                break;

            case R.id.txv_reject:
                showToast("Rejected");
                gotoMainActivity();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        gotoMainActivity();
    }
}
