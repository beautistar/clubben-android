package com.club.security.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.club.security.ClubApplication;
import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.club.security.adapter.SelectedGridViewAdapter;
import com.club.security.adapter.SelectedListViewAdapter;
import com.club.security.commons.Commons;
import com.club.security.commons.Constants;
import com.club.security.commons.ReqConst;
import com.club.security.model.PatronEntity;
import com.club.security.preference.PrefConst;
import com.club.security.preference.Preference;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


@SuppressLint("ValidFragment")
public class PatronsPresentFragment extends Fragment implements View.OnClickListener, SwipyRefreshLayout.OnRefreshListener{

    MainActivity _activity;
    View view;

    ImageView imv_selected_user;
    GridView grd_current_user;
    SelectedGridViewAdapter _grid_adapter;

    ListView lst_selected_user;
    SelectedListViewAdapter _list_adapter;
    TextView txv_submit;

    String _incidentId = "";

    ArrayList<PatronEntity> _all_user = new ArrayList<>();

    String url = "";
    int _curpage = 1;
    SwipyRefreshLayout ui_RefreshLayout;

    public PatronsPresentFragment(MainActivity activity) {
        // Required empty public constructor
        _activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_patrons_present, container, false);

        Commons.ALL_PATRON.clear();
        loadLayout();
        return view;
    }

    private void loadLayout() {

        imv_selected_user = (ImageView)view.findViewById(R.id.imv_selected_user);
        imv_selected_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (url.toString().length() > 0){
                    _activity.gotoPreview(url);

                } else {

                    _activity.showToast("There is no a image");
                }
            }
        });

        ui_RefreshLayout = (SwipyRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        ui_RefreshLayout.setOnRefreshListener(this);
        ui_RefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);

        grd_current_user = (GridView)view.findViewById(R.id.grd_current_user);

        lst_selected_user =  (ListView)view.findViewById(R.id.lst_selected_user);
        _list_adapter = new SelectedListViewAdapter(_activity, this);
        lst_selected_user.setAdapter(_list_adapter);

        txv_submit = (TextView)view.findViewById(R.id.txv_submit);
        txv_submit.setOnClickListener(this);

        _curpage = 1;

        ui_RefreshLayout.post(new Runnable() {

                @Override
                public void run() {
                    getPatrons();
                }
            }
        );
    }

    private void getPatrons(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_PATRONS;
        String params = String.format("/%d/%d/%d", Commons.g_user.get_idx(), Commons.g_user.get_club_id(), _curpage);

        url += params;
        Log.d("getPatronUrl", url);

        ui_RefreshLayout.setRefreshing(true);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parsePatrons(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ui_RefreshLayout.setRefreshing(false);
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parsePatrons(String json) {

        ui_RefreshLayout.setRefreshing(false);

        try {

            _activity.closeProgress();
            JSONObject object  = new JSONObject(json);
            Log.d("getPatronPresent", object.toString());
            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray patron_info = object.getJSONArray("patron_info");

                for (int i = 0; i < patron_info.length(); i++){

                    JSONObject json_patron = patron_info.getJSONObject(i);
                    PatronEntity patron = new PatronEntity();

                    patron.set_id(json_patron.getInt("patron_id"));
                    patron.set_scan_id(json_patron.getString("scan_id"));
                    patron.set_daq_id(json_patron.getString("daq"));
                    patron.set_total_visit_count(json_patron.getInt(ReqConst.RES_TOTAL_VISIT_CNT));
                    patron.set_photo_url(json_patron.getString(ReqConst.RES_PHOTO_URL));

                    _all_user.add(patron);
                }

            } else {

                _activity.closeProgress();
                _activity.showAlertDialog(String.valueOf(result_code));
            }

            _curpage++;

            _grid_adapter = new SelectedGridViewAdapter(_activity, _all_user, PatronsPresentFragment.this);
            grd_current_user.setAdapter(_grid_adapter);
            _grid_adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (ui_RefreshLayout.isRefreshing())
            getPatrons();
    }

    public void setSelectedUser(String user){

        Glide.with(_activity).load(user).placeholder(R.color.white).into(imv_selected_user);

        //if (user.toString().length() > 0){
            url = user;
        //}

    }

    public void addItem(PatronEntity user){

        _list_adapter.addItem(user);
        lst_selected_user.setAdapter(_list_adapter);

        _list_adapter.notifyDataSetChanged();
    }

    public void removeItem(PatronEntity user){

        _list_adapter.removeItem(user);
        lst_selected_user.setAdapter(_list_adapter);

        _list_adapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_submit:
                genIncidentId();
                if (!ui_RefreshLayout.isRefreshing() && Commons.ALL_PATRON.size() > 0)
                _activity.gotoIncidentDescriptionActivity();

                break;
        }
    }

    private void genIncidentId() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = df.format(c.getTime());

        int increaseID = Preference.getInstance().getValue(getActivity(), PrefConst.PREFKEY_INCREASED, 0);
        increaseID++;
        Commons.INCIDENTID = String.format("%d-%s-%d", Commons.g_user.get_club_id(), formattedDate.toString(), increaseID);
        Preference.getInstance().put(getActivity(), PrefConst.PREFKEY_INCREASED, increaseID);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}
