package com.club.security.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.club.security.commons.Constants;

public class ReportIncidentFragment extends Fragment implements View.OnClickListener{

    MainActivity _activity;
    View view;

    TextView txv_present, txv_create;
    public ReportIncidentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_report_incident, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        txv_create = (TextView)view.findViewById(R.id.txv_create);
        txv_create.setOnClickListener(this);

        txv_present = (TextView)view.findViewById(R.id.txv_present);
        txv_present.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_present:

                Constants.CURRENT_PAGE = 2;
                Constants.CREATE = 3;
                _activity.scanPDF417();

                break;

            case R.id.txv_create:
                Constants.CREATE = 5;
                _activity.gotoPatronsPresentFragment();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.CURRENT_PAGE = 0;
        //Constants.CREATE = 0;
    }
}
