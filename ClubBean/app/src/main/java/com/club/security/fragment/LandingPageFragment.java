package com.club.security.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.club.security.ClubApplication;
import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.club.security.commons.Constants;
import com.club.security.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class LandingPageFragment extends Fragment {

    MainActivity _activity;
    View view;

    TextView txv_patron_num,txv_security_num, txv_incident_num;


    public LandingPageFragment(MainActivity activity) {
        // Required empty public constructor
        this._activity = activity;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Constants.DESCRIPTION = "";
        view = inflater.inflate(R.layout.fragment_landing_page, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        txv_patron_num = (TextView)view.findViewById(R.id.txv_patron_num);
        txv_security_num =(TextView)view.findViewById(R.id.txv_security_num);
        txv_incident_num =(TextView)view.findViewById(R.id.txv_incident_num);

        ImageView imv_landing = (ImageView)view.findViewById(R.id.imv_landing);
        imv_landing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.CURRENT_PAGE = 1;
                //_activity.gotoScan();

               // _activity.scanPdf417();
               // _activity.gotoScan();

                _activity.scanPDF417();
            }
        });

        getRecords();
    }

    private void getRecords(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_RECORDS;
        //_activity.showProgress();
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseRecords(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseRecords(String json){

        _activity.closeProgress();

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);
            if (result_code == ReqConst.CODE_SUCCESS){

                txv_patron_num.setText(response.getString(ReqConst.RES_PATRON_NUM));
                txv_security_num.setText(response.getString(ReqConst.RES_SECURITY_NUM));
                txv_incident_num.setText(response.getString(ReqConst.RES_INCIDENT_NUM));
            }

        } catch (JSONException e) {
            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        Constants.CURRENT_PAGE = 0;
    }
}
