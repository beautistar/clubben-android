package com.club.security.model;

import java.io.Serializable;

/**
 * Created by HugeRain on 5/23/2017.
 */

public class StateEntity implements Serializable {

    int _id = 0;
    String _name = "";
    int _iin = 0;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public int get_iin() {
        return _iin;
    }

    public void set_iin(int _iin) {
        this._iin = _iin;
    }
}
