package com.club.security.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.club.security.ClubApplication;
import com.club.security.R;
import com.club.security.base.CommonActivity;
import com.club.security.commons.Commons;
import com.club.security.commons.Constants;
import com.club.security.commons.ReqConst;
import com.club.security.model.PatronEntity;
import com.club.security.utils.BitmapUtils;
import com.club.security.utils.MultiPartRequest;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SubmissionActivity extends CommonActivity implements View.OnClickListener {

    ImageView imv_selected_user;
    TextView txv_cancel, txv_edit, txv_confirm;
    EditText txv_description;

    ArrayList<PatronEntity> _patrons = new ArrayList<>();
    ArrayList<String> stringStr = new ArrayList<>();

    PatronEntity selectedPatron = new PatronEntity();

    MaterialSpinner spinner;

    private Uri _imageCaptureUri;
    String _photoPath = "";

    String _incident_type = "";
    String _incident_description = "";
    int _patron_id = 0;

    int selectedIndex = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission);

        if (Constants.CREATE == 3){

            _patron_id = getIntent().getIntExtra(Constants.KEY_PATRON_ID, 0);
            _photoPath = getIntent().getStringExtra(Constants.KEY_PHOTO_URL);
        }

        _incident_description = getIntent().getStringExtra(Constants.KEY_DESCRIPTION);

        Constants._from = Constants.FROM_SUBMISSION;

        loadLayout();
    }

    private void loadLayout() {

        imv_selected_user = (ImageView)findViewById(R.id.imv_selected_user);

        txv_cancel = (TextView)findViewById(R.id.txv_cancel);
        txv_cancel.setOnClickListener(this);

        txv_edit = (TextView)findViewById(R.id.txv_edit);
        txv_edit.setOnClickListener(this);

        txv_confirm = (TextView)findViewById(R.id.txv_confirm);
        txv_confirm.setOnClickListener(this);

        txv_description = (EditText) findViewById(R.id.txv_description);
        txv_description.setEnabled(false);
        txv_description.setText(_incident_description);

        spinner = (MaterialSpinner)findViewById(R.id.spinner);


        if (Constants.CREATE == 3){

            spinner.setVisibility(View.GONE);

            if (_photoPath.toString().length() > 0)
                Glide.with(this).load(_photoPath).placeholder(R.drawable.bg_non_profile).into(imv_selected_user);

            //multi patron
        } else {

            spinner.setVisibility(View.VISIBLE);
            for (int i = 0; i < Commons.ALL_PATRON.size() ; i++){

                _patrons.add(Commons.ALL_PATRON.get(i));
                stringStr.add(_patrons.get(i).get_daq_id());
            }

            spinner.setItems(stringStr);

            spinner.setSelectedIndex(Constants.selectedSpinnerIndex);

            // first time, selected patron is index 0 of strings.
            selectedPatron = _patrons.get(Constants.selectedSpinnerIndex);
            _patron_id = selectedPatron.get_id();

            Log.d("=======>StartURL==>", selectedPatron.get_photo_url());
            if (selectedPatron.get_photo_url().toString().length() > 0)
                Glide.with(this).load(selectedPatron.get_photo_url()).placeholder(R.drawable.bg_non_profile).into(imv_selected_user);
        }

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

                selectedPatron = _patrons.get(position);
                if (selectedPatron.get_photo_url().length() > 0)
                    Glide.with(getApplicationContext()).load(selectedPatron.get_photo_url()).placeholder(R.drawable.bg_non_profile).into(imv_selected_user);
                _patron_id = selectedPatron.get_id();
            }
        });

    }

    private void gotoIncidentActivity() {

        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    private void submitIncident(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SUBMIT_INCIDENT;

        showProgress();

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseSubmitIncident(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (isFinishing()){
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String , String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_CLUBID, String.valueOf(Commons.g_user.get_club_id()));
                    params.put(ReqConst.RES_PATRON_ID, String.valueOf(_patron_id));
                    params.put("type", String.valueOf("1,2,3"));
                    params.put("description", txv_description.getText().toString());
                    params.put("incident_id", Commons.INCIDENTID);

                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseSubmitIncident(String response) {

        Log.d("Response==>", response);

        try {
            closeProgress();
            JSONObject object = new JSONObject(response);
            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){


                showToast("Success submit");

                if (Constants.CREATE == 3){

                    gotoMain();

                    Commons.PATRON_ID = 0;
                    Commons.PHOTO_PATH = "";
                    Constants.DESCRIPTION = "";
                    Constants.KEEP_DESCRIPTION = "";
                    Constants.CREATE = 0;
                }

                else {

                    // stringstr clear
                    stringStr.clear();

                    _patrons.remove(selectedPatron);
                    Commons.ALL_PATRON.remove(selectedPatron);

                    if (_patrons.size() > 0) {

                        Constants.selectedSpinnerIndex = 0;

                        for (int i = 0 ; i < _patrons.size() ; i++) {

                            stringStr.add(_patrons.get(i).get_daq_id());
                        }

                        selectedPatron = _patrons.get(0);
                        _patron_id = selectedPatron.get_id();
                        spinner.setText(stringStr.get(0));
                        spinner.setItems(stringStr);
                        Log.d("Response_after==URL===>", selectedPatron.get_photo_url());
                        Glide.with(this).load(selectedPatron.get_photo_url()).placeholder(R.drawable.bg_non_profile).into(imv_selected_user);
                        txv_description.setText(Constants.KEEP_DESCRIPTION);

                    } else {

                        Constants.selectedSpinnerIndex = 0;

                        _patron_id = 0;
                        txv_description.setText("");
                        _photoPath = "";
                        imv_selected_user.setImageResource(R.drawable.bg_non_profile);
                        spinner.setText("");
                        Constants.DESCRIPTION = "";
                        Constants.KEEP_DESCRIPTION = "";
                        Constants.CREATE = 0;
                        spinner.setVisibility(View.GONE);

                        gotoMain();
                    }

                }

            } else {

                closeProgress();
                showAlertDialog(String.valueOf(result_code));
            }
        } catch (JSONException e) {

            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }

    }

    public void gotoMain(){

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private boolean checkValid(){

        if (_patron_id == 0){

            showAlertDialog("There is no patron");
            return false;

        } else if (txv_description.getText().toString().length() == 0){

            showAlertDialog("Please input the description.");
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_cancel:

                Constants.selectedSpinnerIndex = 0;
                Commons.ALL_PATRON = new ArrayList<>();
                Commons.PATRON_ID = 0;
                Commons.PHOTO_PATH = "";
                Constants.DESCRIPTION = "";
                Constants.KEEP_DESCRIPTION = "";
                Constants.CREATE = 0;
                gotoMain();
                break;

            case R.id.txv_edit:

                Constants.selectedSpinnerIndex = spinner.getSelectedIndex();
                startActivity(new Intent(this, IncidentDescriptionActivity.class));
                finish();
                break;

            case R.id.txv_confirm:

                if (checkValid()){
                    submitIncident();
                }

                break;
        }

    }

    @Override
    public void onBackPressed() {
        Constants.DESCRIPTION = "";
        Constants.KEEP_DESCRIPTION = "";
        Constants.CREATE = 0;
        gotoIncidentActivity();
    }
}
