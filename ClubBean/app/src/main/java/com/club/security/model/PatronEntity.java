package com.club.security.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HugeRain on 4/28/2017.
 */

public class PatronEntity implements Serializable {

    int _id = 0;
    String _scan_id = "";
    String _daq_id = "";
    String _name = "";
    int _total_visit_count = 0;
    int _total_incident_count = 0;
    String _error_des = "";
    String _photo_url = "";
    String _last_present_date = "";
    String _registed_date = "";


    ArrayList<IncidentEntity> _incident_list = new ArrayList<>();


    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_scan_id() {
        return _scan_id;
    }

    public void set_scan_id(String _scan_id) {
        this._scan_id = _scan_id;
    }

    public String get_daq_id() {
        return _daq_id;
    }

    public void set_daq_id(String _daq_id) {
        this._daq_id = _daq_id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_error_des() {
        return _error_des;
    }

    public void set_error_des(String _error_des) {
        this._error_des = _error_des;
    }

    public String get_photo_url() {
        return _photo_url;
    }

    public void set_photo_url(String _photo_url) {
        this._photo_url = _photo_url;
    }

    public int get_total_visit_count() {
        return _total_visit_count;
    }

    public void set_total_visit_count(int _total_visit_count) {
        this._total_visit_count = _total_visit_count;
    }

    public int get_total_incident_count() {
        return _total_incident_count;
    }

    public void set_total_incident_count(int _total_incident_count) {
        this._total_incident_count = _total_incident_count;
    }

    public String get_last_present_date() {
        return _last_present_date;
    }

    public void set_last_present_date(String _last_present_date) {
        this._last_present_date = _last_present_date;
    }

    public String get_registed_date() {
        return _registed_date;
    }

    public void set_registed_date(String _registed_date) {
        this._registed_date = _registed_date;
    }

    public ArrayList<IncidentEntity> get_incident_list() {
        return _incident_list;
    }

    public void set_incident_list(ArrayList<IncidentEntity> _incident_list) {
        this._incident_list = _incident_list;
    }
}
