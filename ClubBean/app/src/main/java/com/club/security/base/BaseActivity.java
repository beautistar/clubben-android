package com.club.security.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.club.security.R;
import com.club.security.activity.EvaluationActivity;
import com.club.security.activity.IncidentDescriptionActivity;
import com.club.security.activity.MainActivity;
import com.club.security.activity.ScannerActivity;
import com.club.security.commons.Commons;
import com.club.security.commons.Constants;
import com.microblink.Config;
import com.microblink.activity.Pdf417ScanActivity;
import com.microblink.activity.ScanActivity;
import com.microblink.activity.ScanCard;
import com.microblink.activity.SegmentScanActivity;
import com.microblink.activity.ShowOcrResultMode;
import com.microblink.recognizers.BaseRecognitionResult;
import com.microblink.recognizers.RecognitionResults;
import com.microblink.recognizers.blinkbarcode.BarcodeType;
import com.microblink.recognizers.blinkbarcode.bardecoder.BarDecoderRecognizerSettings;
import com.microblink.recognizers.blinkbarcode.bardecoder.BarDecoderScanResult;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417RecognizerSettings;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417ScanResult;
import com.microblink.recognizers.blinkbarcode.simnumber.SimNumberRecognizerSettings;
import com.microblink.recognizers.blinkbarcode.zxing.ZXingRecognizerSettings;
import com.microblink.recognizers.blinkbarcode.zxing.ZXingScanResult;
import com.microblink.recognizers.settings.RecognitionSettings;
import com.microblink.recognizers.settings.RecognizerSettings;
import com.microblink.results.barcode.BarcodeDetailedData;
import com.microblink.util.RecognizerCompatibility;
import com.microblink.util.RecognizerCompatibilityStatus;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by HGS on 12/11/2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements
        Handler.Callback {

    public Context _context = null;

    public Handler _handler = null;

    private ProgressDialog _progressDlg;

    private Vibrator _vibrator;

    String _iin ="", _daq = "", _dba = "", _dbb = "";
    int _current_page = 0;

    public static final int MY_BLINKID_REQUEST_CODE = 0x101;
    private ListElement[] mElements;

    //PDF417
    private static final String LICENSE_KEY = "VRDCJSDH-L5KMHQHA-SBIWQMA7-7VE2IALJ-YVH5DJ46-QJPM3GAH-QZ3GVCCR-MTEVYE5L";
    private static final int MY_REQUEST_CODE = 1337;

    private static final String TAG = "Pdf417MobiDemo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _context = this;

        _vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        _handler = new Handler(this);





    }

    public void scanPDF417(){

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        RecognizerCompatibilityStatus supportStatus = RecognizerCompatibility.getRecognizerCompatibilityStatus(this);
        if (supportStatus != RecognizerCompatibilityStatus.RECOGNIZER_SUPPORTED) {
            Toast.makeText(this, "BlinkID is not supported! Reason: " + supportStatus.name(), Toast.LENGTH_LONG).show();
        }

        // build list elements
        buildElements();
        startActivityForResult(mElements[0].getScanIntent(), MY_BLINKID_REQUEST_CODE);

    }

    private Intent buildIntent(RecognizerSettings[] settArray, Class<?> target, Intent helpIntent) {
        final Intent intent = new Intent(this, target);

        intent.putExtra(ScanActivity.EXTRAS_BEEP_RESOURCE, R.raw.beep);

        if (helpIntent != null) {
            intent.putExtra(ScanActivity.EXTRAS_HELP_INTENT, helpIntent);
        }

        RecognitionSettings settings = new RecognitionSettings();

        settings.setNumMsBeforeTimeout(2000);

        settings.setRecognizerSettingsArray(settArray);
        intent.putExtra(ScanActivity.EXTRAS_RECOGNITION_SETTINGS, settings);

        intent.putExtra(ScanActivity.EXTRAS_LICENSE_KEY, Config.LICENSE_KEY);

        intent.putExtra(ScanActivity.EXTRAS_SHOW_FOCUS_RECTANGLE, true);


        intent.putExtra(ScanActivity.EXTRAS_ALLOW_PINCH_TO_ZOOM, true);

        intent.putExtra(SegmentScanActivity.EXTRAS_SHOW_OCR_RESULT_MODE, (Parcelable) ShowOcrResultMode.ANIMATED_DOTS);

        return intent;
    }

    private void buildElements() {

        ArrayList<ListElement> elements = new ArrayList<ListElement>();

        elements.add(buildPDF417Element());

        mElements = new ListElement[elements.size()];
        elements.toArray(mElements);
    }


    private ListElement buildPDF417Element() {

        // prepare settings for PDF417 barcode recognizer
        Pdf417RecognizerSettings pdf417 = new Pdf417RecognizerSettings();

        Intent intent = buildIntent(new RecognizerSettings[]{pdf417}, Pdf417ScanActivity.class, null);
        intent.putExtra(Pdf417ScanActivity.EXTRAS_SHOW_DIALOG_AFTER_SCAN, false);

        return new ListElement("PDF417 barcode", intent);
    }

    private class ListElement {

        private String mTitle;
        private Intent mScanIntent;

        public String getTitle() {
            return mTitle;
        }

        public Intent getScanIntent() {
            return mScanIntent;
        }

        public ListElement(String title, Intent scanIntent) {
            mTitle = title;
            mScanIntent = scanIntent;
        }

        @Override
        public String toString() {
            return getTitle();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {

        closeProgress();

        try {
            if (_vibrator != null)
                _vibrator.cancel();
        } catch (Exception e) {
        }
        _vibrator = null;

        super.onDestroy();
    }


    public void showProgress(boolean cancelable) {

        closeProgress();

        _progressDlg = new ProgressDialog(_context, R.style.MyTheme);
        _progressDlg
                .setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        _progressDlg.setCancelable(cancelable);
        _progressDlg.show();

    }

    public void showProgress() {
        showProgress(false);
    }

    public void closeProgress() {

        if(_progressDlg == null) {
            return;
        }

        _progressDlg.dismiss();
        _progressDlg = null;
    }

    public void showAlertDialog(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    /**
     *  show toast
     * @param toast_string
     */
    public void showToast(String toast_string) {

        Toast.makeText(_context, toast_string, Toast.LENGTH_SHORT).show();
    }

    public void vibrate() {

        if (_vibrator != null)
            _vibrator.vibrate(500);
    }

    @Override
    public boolean handleMessage(Message msg) {

        switch (msg.what) {

            default:
                break;
        }

        return false;
    }

    public boolean _isEndFlag = false;

    public static final int BACK_TWO_CLICK_DELAY_TIME = 3000; // ms

    public Runnable _exitRunner = new Runnable() {

        @Override
        public void run() {

            _isEndFlag = false;
        }
    };

    public void onExit() {

        if (_isEndFlag == false) {

            Toast.makeText(this, getString(R.string.str_back_one_more_end),
                    Toast.LENGTH_SHORT).show();
            _isEndFlag = true;

            _handler.postDelayed(_exitRunner, BACK_TWO_CLICK_DELAY_TIME);

        } else if (_isEndFlag == true) {

            Commons.g_isAppRunning = false;
            finish();
        }
    }

    public void showAlertDialog2(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.try_again),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        gotoMain();
                    }
                });
        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    public void showAlertDialog1(String msg, final int current_page) {

        final AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle("List of scan results");

        String message = "IIN : " + getIIN(msg) + "\n\n"
                + "DAQ : " + getDAQ(msg) + "\n\n"
                + "DBA : " + getDBA(msg) + "\n\n"
                + "DBB : " + getDBB(msg) ;

        alertDialog.setMessage(message);

        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.submit),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        Log.d("Current_page+>", String.valueOf(Constants.CURRENT_PAGE));

                        if (current_page == 1){

                            //_activity.gotoIndentFragment();

                            Intent intent = new Intent(BaseActivity.this, EvaluationActivity.class);

                            intent.putExtra(Constants.KEY_IIN, _iin);
                            intent.putExtra(Constants.KEY_DAQ, _daq);
                            intent.putExtra(Constants.KEY_DBA, _dba);
                            intent.putExtra(Constants.KEY_DBB, _dbb);

                            Log.d("IIN===>", _iin);
                            Log.d("DAQ===>", _daq);
                            Log.d("DBA===>", _dba);

                            startActivity(intent);
                            overridePendingTransition(0,0);
                            finish();

                        } else if (current_page == 2){

                            Intent intent = new Intent(BaseActivity.this, IncidentDescriptionActivity.class);

                            Constants._from = Constants.FROM_SCAN;

                            intent.putExtra(Constants.KEY_IIN,_iin);
                            intent.putExtra(Constants.KEY_DAQ, _daq);
                            intent.putExtra(Constants.KEY_DBA, _dba);
                            intent.putExtra(Constants.KEY_DBB, _dbb);

                            startActivity(intent);
                            overridePendingTransition(0,0);
                            finish();

                        } else if (current_page == 3){

                            Intent intent = new Intent(BaseActivity.this, MainActivity.class);

                            Constants._from = Constants.FROM_SCAN_DISPUTE;

                            intent.putExtra(Constants.KEY_IIN,_iin);
                            intent.putExtra(Constants.KEY_DAQ, _daq);
                            intent.putExtra(Constants.KEY_DBA, _dba);
                            intent.putExtra(Constants.KEY_DBB, _dbb);

                            startActivity(intent);
                            overridePendingTransition(0,0);
                            finish();

                        } else {

                            return;
                        }

                        finish();

                    }
                });

      /*  alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,  _context.getString(R.string.cancel),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        alertDialog.dismiss();
                        gotoMain();
                    }
                });
*/
        alertDialog.show();
    }

    public void gotoMain(){

        startActivity(new Intent(BaseActivity.this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    private String getIIN(String str) {

        try {

            Pattern pattern = Pattern.compile("(\\d{6})");

            //   \d is for a digit
            //   {} is the number of digits here 6.

            Matcher matcher = pattern.matcher(str);

            if (matcher.find()) {
                _iin = matcher.group(1);  // 6 digit number
                Log.d("6 IIN : ", _iin);
            }

        } catch (Exception e) {

            e.printStackTrace();
//            showAlertDialog("Invalid ID");
            return "N/A";
        }

        return _iin;
    }

    // get DAQ : License Number
    private String getDAQ(String str) {


        try {

            String splitedStr = str.split("DAQ")[1]; // cut "DAQ" before string
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(splitedStr);

            while (m.find()) {
                _daq = m.group();
                break;
            }


        } catch (Exception e) {

            e.printStackTrace();
//            showAlertDialog("Invalid ID");
            return "N/A";
        }

        return _daq;
    }

    // get DBA  from scanned string : Expiration Date
    private String getDBA(String str) {

        try {

            String splitedStr = str.split("DBA")[1];

            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(splitedStr);

            while (m.find()) {
                _dba = m.group();
                Log.d("DBA : ", m.group());
                break;
            }
        } catch (Exception e) {

            return "N/A";
//            showAlertDialog("Invalid ID");
        }

        return _dba;
    }

    // get DBB DBB: Date of Birth
    private String getDBB(String str) {


        try {

            String splitedStr = str.split("DBB")[1];

            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(splitedStr);

            while (m.find()) {
                _dbb = m.group();
                Log.d("DBB : ", m.group());
                break;
            }

        } catch (Exception e) {

            return "N/A";
//            showAlertDialog("Invalid ID");
        }

        return _dbb;
    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

   /* public void scanPdf417() {

        com.microblink.util.Log.i(TAG, "scan will be performed");
        // Intent for ScanActivity
        Intent intent = new Intent(this, Pdf417ScanActivity.class);
        intent.putExtra(Pdf417ScanActivity.EXTRAS_LICENSE_KEY, LICENSE_KEY);
        Pdf417RecognizerSettings pdf417RecognizerSettings = new Pdf417RecognizerSettings();
        pdf417RecognizerSettings.setNullQuietZoneAllowed(true);
        BarDecoderRecognizerSettings oneDimensionalRecognizerSettings = new BarDecoderRecognizerSettings();
        oneDimensionalRecognizerSettings.setScanCode39(true);
        oneDimensionalRecognizerSettings.setScanCode128(true);
        ZXingRecognizerSettings zXingRecognizerSettings = new ZXingRecognizerSettings();
        zXingRecognizerSettings.setScanQRCode(true);
        zXingRecognizerSettings.setScanITFCode(true);

        SimNumberRecognizerSettings sm = new SimNumberRecognizerSettings();

        RecognitionSettings recognitionSettings = new RecognitionSettings();

        recognitionSettings.setRecognizerSettingsArray(new RecognizerSettings[]{pdf417RecognizerSettings, oneDimensionalRecognizerSettings, zXingRecognizerSettings,sm});

        intent.putExtra(Pdf417ScanActivity.EXTRAS_RECOGNITION_SETTINGS, recognitionSettings);
        intent.putExtra(Pdf417ScanActivity.EXTRAS_SHOW_DIALOG_AFTER_SCAN, false);

        intent.putExtra(Pdf417ScanActivity.EXTRAS_ALLOW_PINCH_TO_ZOOM, true);

        intent.putExtra(Pdf417ScanActivity.EXTRAS_SHOW_FOCUS_RECTANGLE, true);

        startActivityForResult(intent, MY_REQUEST_CODE);
    }*/

   /* private boolean checkIfDataIsUrlAndCreateIntent(String data) {


        // if barcode contains URL, create intent for browser
        // else, contain intent for message
        boolean barcodeDataIsUrl;
        try {
            @SuppressWarnings("unused")
            URL url = new URL(data);
            barcodeDataIsUrl = true;
        } catch (MalformedURLException exc) {
            barcodeDataIsUrl = false;
        }

        if (barcodeDataIsUrl) {
            // create intent for browser
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(data));
            startActivity(Intent.createChooser(intent, getString(R.string.UseWith)));
        }
        return barcodeDataIsUrl;
    }*/

    /*@Override
    protected void onActivityResult1(int requestCode, int resultCode, Intent data) {

        if (requestCode == MY_REQUEST_CODE && resultCode == Pdf417ScanActivity.RESULT_OK) {

            RecognitionResults results = data.getParcelableExtra(Pdf417ScanActivity.EXTRAS_RECOGNITION_RESULTS);
            // Get scan results array. If scan was successful, array will contain at least one element.
            // Multiple element may be in array if multiple scan results from single image were allowed in settings.
            BaseRecognitionResult[] resultArray = results.getRecognitionResults();

            Log.d("===resultArray==>", String.valueOf(resultArray));

            // Each recognition result corresponds to active recognizer. As stated earlier, there are 3 types of
            // recognizers available (PDF417, Bardecoder and ZXing), so there are 3 types of results
            // available.

            StringBuilder sb = new StringBuilder();

            for(BaseRecognitionResult res : resultArray) {
                if(res instanceof Pdf417ScanResult) { // check if scan result is result of Pdf417 recognizer
                    Pdf417ScanResult result = (Pdf417ScanResult) res;
                    // getStringData getter will return the string version of barcode contents
                    String barcodeData = result.getStringData();

                    Log.d("String++Result==>", barcodeData);
                    // isUncertain getter will tell you if scanned barcode contains some uncertainties
                    boolean uncertainData = result.isUncertain();
                    // getRawData getter will return the raw data information object of barcode contents
                    BarcodeDetailedData rawData = result.getRawData();
                    // BarcodeDetailedData contains information about barcode's binary layout, if you
                    // are only interested in raw bytes, you can obtain them with getAllData getter
                    byte[] rawDataBuffer = rawData.getAllData();

                    // if data is URL, open the browser and stop processing result
                    if(checkIfDataIsUrlAndCreateIntent(barcodeData)) {
                        return;
                    } else {
                        // add data to string builder
                        sb.append("PDF417 scan data");
                        if (uncertainData) {
                            sb.append("This scan data is uncertain!\n\n");
                        }
                        sb.append(" string data:\n");
                        sb.append(barcodeData);
                        if (rawData != null) {
                            sb.append("\n\n");
                            sb.append("PDF417 raw data:\n");
                            sb.append(rawData.toString());
                            sb.append("\n");
                            sb.append("PDF417 raw data merged:\n");
                            sb.append("{");
                            if (rawDataBuffer.length > 0)
                            for (int i = 0; i < rawDataBuffer.length; ++i) {
                                sb.append((int) rawDataBuffer[i] & 0x0FF);
                                if (i != rawDataBuffer.length - 1) {
                                    sb.append(", ");
                                }
                            }
                            sb.append("}\n\n\n");
                        }
                    }
                } else if(res instanceof BarDecoderScanResult) { // check if scan result is result of BarDecoder recognizer
                    BarDecoderScanResult result = (BarDecoderScanResult) res;
                    // with getBarcodeType you can obtain barcode type enum that tells you the type of decoded barcode
                    BarcodeType type = result.getBarcodeType();
                    // as with PDF417, getStringData will return the string contents of barcode
                    String barcodeData = result.getStringData();
                    if(checkIfDataIsUrlAndCreateIntent(barcodeData)) {
                        return;
                    } else {
                        sb.append(type.name());
                        sb.append(" string data:\n");
                        sb.append(barcodeData);
                        sb.append("\n\n\n");
                    }
                } else if(res instanceof ZXingScanResult) { // check if scan result is result of ZXing recognizer
                    ZXingScanResult result= (ZXingScanResult) res;
                    // with getBarcodeType you can obtain barcode type enum that tells you the type of decoded barcode
                    BarcodeType type = result.getBarcodeType();
                    // as with PDF417, getStringData will return the string contents of barcode
                    String barcodeData = result.getStringData();

                    Log.d("Sub_BarCode_Data==>", barcodeData);
                    if(checkIfDataIsUrlAndCreateIntent(barcodeData)) {
                        return;
                    } else {
                        sb.append(type.name());
                        sb.append(" string data:\n");
                        sb.append(barcodeData);
                        sb.append("\n\n\n");
                    }
                }
            }

            Log.d("COnstantCUrrentPage==", String.valueOf(Constants.CURRENT_PAGE));


            if (((getIIN(sb.toString())).equals("N/A") || (getDAQ(sb.toString())).equals("N/A") || (getDBA(sb.toString())).equals("N/A") || (getDBB(sb.toString())).equals("N/A"))){

                showAlertDialog2("Invalid number");

            } else {

                showAlertDialog1(sb.toString(), _current_page = Constants.CURRENT_PAGE);
            }
        }
    }
*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // onActivityResult is called whenever we are returned from activity started
        // with startActivityForResult. We need to check request code to determine
        // that we have really returned from BlinkID activity.

        if (requestCode == MY_BLINKID_REQUEST_CODE) {

            // make sure BlinkID activity returned result
            if (resultCode == Activity.RESULT_OK && data != null) {


                Bundle extras = data.getExtras();

                // //////////////////////pdf471

                RecognitionResults results = data.getParcelableExtra(ScanCard.EXTRAS_RECOGNITION_RESULTS);


                BaseRecognitionResult[] dataArray = results.getRecognitionResults();
                for(BaseRecognitionResult baseResult : dataArray) {
                    if(baseResult instanceof Pdf417ScanResult) {
                        Pdf417ScanResult result = (Pdf417ScanResult) baseResult;

                        // getStringData getter will return the string version of barcode contents
                        String barcodeData = result.getStringData();

                        Log.d("pdf417", barcodeData);

                        Log.d("COnstantCUrrentPage==", String.valueOf(Constants.CURRENT_PAGE));


                        if (((getIIN(barcodeData.toString())).equals("N/A") || (getDAQ(barcodeData.toString())).equals("N/A") || (getDBA(barcodeData.toString())).equals("N/A") || (getDBB(barcodeData.toString())).equals("N/A"))){

                            showAlertDialog2("Invalid number");

                        } else {

                            showAlertDialog1(barcodeData.toString(), _current_page = Constants.CURRENT_PAGE);
                        }

                    }
                }

            } else {
                // if BlinkID activity did not return result, user has probably
                // pressed Back button and cancelled scanning
                Toast.makeText(this, "Scan cancelled!", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
