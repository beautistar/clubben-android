package com.club.security.commons;

import com.club.security.base.CommonActivity;

import java.util.logging.Handler;


public class ReqConst {

    public static final String SERVER_ADDR = "http://54.148.154.173";  //http://54.148.154.173/index.php/api/
    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static Handler g_handler = null;
    public static String g_appVersion = "1.0";


    //public static final String SERVER_ADDR = "http://192.168.1.186/clubben";


    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api/";

    //Request value
    public static final String REQ_SINGUP = "signup";
    public static final String REQ_LOGIN = "login";
    public static final String REQ_GET_EVALUATION = "getEvaluation";
    public static final String REQ_GET_DISPUTE_INCIDENT = "getDisputeIncidentListByScan";
    public static final String REQ_ACCEPT_PATRON_PHOTO = "acceptPatronWithPhoto";
    public static final String REQ_GET_PATRON_BYSCAN = "getPatronByScan";
    public static final String REQ_SUBMIT_DISPUTE = "submitDispute";
    public static final String REQ_GET_PATRONS = "getPatrons";
    public static final String REQ_GET_RECORDS = "getRecords";

    //Request Params

    public static final String PARAM_FILE = "file";


    //response value

    /*LOGIN*/
    public static final String RES_USER_INFO = "user_info";
    public static final String RES_USER_ID = "idx";
    public static final String RES_USERNAME = "username";
    public static final String RES_EMAIL = "email";
    public static final String RES_PERMISSION = "permission";
    public static final String RES_CLUBID = "club_id";
    public static final String RES_CLUBNAME = "club_name";

    /*getEvaluation*/
    public static final String RES_PATRON_ID = "patron_id";
    public static final String RES_TOTAL_VISIT_CNT = "total_visit_count";
    public static final String RES_TOTAL_INCIDENT_CNT = "total_incident_count";
    public static final String RES_PHOTO_URL = "photo_url";
    public static final String RES_LAST_PRESENT_DATE = "last_present_date";
    public static final String RES_REGISTERED_DATA = "registered_date";
    public static final String RES_INCIDENT_LIST = "incident_list";
    public static final String RES_INCIDENT_ID = "incident_id";
    public static final String RES_INCIDENT_DES = "incident_description";
    public static final String RES_INCIDENT_PICTURE = "incident_picture";
    public static final String RES_INCIDENT_TYPE = "incident_type";
    public static final String RES_INCIDENT_DATE = "incident_date";
    public static final String RES_INCIDENT_DISPUT_STATUS = "incident_dispute_status";

    //LandingPage
    public static final String RES_PATRON_NUM = "patron_number";
    public static final String RES_SECURITY_NUM = "security_number";
    public static final String RES_INCIDENT_NUM = "incident_number";

    /*Submission*/
    public static final String REQ_SUBMIT_INCIDENT = "submitIncident";

    public static final String RES_CODE = "result_code";


    public static final int CODE_SUCCESS = 0;
    public static final int CODE_UNREGISTER_EMAIL = 201;
    public static final int CODE_WRONG_PASSWORD = 202;
    public static final int CODE_WAIT = 205;
    public static final int CODE_UNKNOWN = 204;
    //public static final int CODE_SUCCESS = 0;


}
