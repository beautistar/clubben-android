package com.club.security.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.club.security.R;
import com.club.security.base.CommonActivity;
import com.club.security.commons.Commons;
import com.club.security.commons.Constants;
import com.club.security.fragment.AboutUsFragment;
import com.club.security.fragment.DisputIncidentFragment;
import com.club.security.fragment.DisputeDetailsFragment;
import com.club.security.fragment.LandingPageFragment;
import com.club.security.fragment.ReportIncidentFragment;
import com.club.security.fragment.PatronsPresentFragment;
import com.club.security.model.IncidentEntity;
import com.club.security.preference.PrefConst;
import com.club.security.preference.Preference;

import static com.club.security.R.id.frm_container;

public class MainActivity extends CommonActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{


    NavigationView ui_drawer_menu;
    DrawerLayout ui_drawerlayout;
    ActionBarDrawerToggle drawerToggle;
    ImageView ui_imv_call_drawer;

    View header_view;
    TextView txv_title;

    String _iin = "", _daq = "", _dba = "", _dbb = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkLocationPermission();
        loadLayout();
    }

    private void loadLayout() {

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        ui_imv_call_drawer = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_drawer.setOnClickListener(this);

        ui_drawer_menu = (NavigationView)findViewById(R.id.drawer_menu);
        ui_drawer_menu.setNavigationItemSelectedListener(this);

        header_view = ui_drawer_menu.getHeaderView(0);

        ui_drawerlayout.addDrawerListener(drawerToggle);

        txv_title = (TextView)findViewById(R.id.txv_title);

        //Commons.g_user.get_permission() == 2

        Log.d("Security===>", String.valueOf(Constants.SECURITY));

//        if (Commons.g_user.get_permission() == 3){
//
//            ui_imv_call_drawer.setVisibility(View.GONE);
//
//            ui_drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//        }

        setupNavigationBar();

        if (Constants._from == Constants.FROM_SCAN_DISPUTE){

            Constants._from = 0;

            if (getIntent().getStringExtra(Constants.KEY_IIN) == null || getIntent().getStringExtra(Constants.KEY_DAQ) == null || getIntent().getStringExtra(Constants.KEY_DBA) == null || getIntent().getStringExtra(Constants.KEY_DBB) == null)

                gotoDisputeFragment();

            else {

                _iin = getIntent().getStringExtra(Constants.KEY_IIN);
                _daq = getIntent().getStringExtra(Constants.KEY_DAQ);
                _dba = getIntent().getStringExtra(Constants.KEY_DBA);
                _dbb = getIntent().getStringExtra(Constants.KEY_DBB);

                gotoDisputeFragment(_iin, _daq, _dba, _dbb);

            }

        } else if (Constants.CURRENT_PAGE == 2){

            gotoReportIncidentFragment();
        } else {

            gotoLandingPageFragment();
        }

    }

    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, ui_drawerlayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);}

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);}
        };

        ui_drawerlayout.setDrawerListener(drawerToggle);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_call_drawer:
                ui_drawerlayout.openDrawer(Gravity.LEFT);
                break;
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        ui_drawerlayout.closeDrawers();

        switch (id){

            case R.id.nav_landing:
                gotoLandingPageFragment();
                break;

            case R.id.nav_report:
                gotoReportIncidentFragment();
                break;

            case R.id.nav_dispute:
                gotoDisputeFragment();
                break;

            case R.id.nav_about_us:
                //gotoPatronsPresentFragment();

                gotoAboutUsFragment();
                break;

            case R.id.nav_logout:

                Intent intent = new Intent(this, LoginActivity.class);
                intent.putExtra(Constants.KEY_LOGOUT, true);
                startActivity(intent);
                finish();
                break;
        }

        return false;
    }

    public void gotoLandingPageFragment(){

        txv_title.setText("Evaluation");

        LandingPageFragment fragment = new LandingPageFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void gotoReportIncidentFragment(){

        txv_title.setText(getString(R.string.report_incident));

        ReportIncidentFragment fragment = new ReportIncidentFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void gotoDisputeFragment(String IIN, String DAQ, String DBA, String DBB){

        txv_title.setText(getString(R.string.dispute_incident));

        DisputIncidentFragment fragment = new DisputIncidentFragment(this, IIN, DAQ, DBA, DBB);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void gotoDisputeFragment(){

        txv_title.setText(getString(R.string.dispute_incident));

        DisputIncidentFragment fragment = new DisputIncidentFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void gotoPatronsPresentFragment(){

        txv_title.setText(getString(R.string.patron_present));

        PatronsPresentFragment fragment = new PatronsPresentFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void gotoAboutUsFragment(){

        txv_title.setText(getString(R.string.about_us));

        AboutUsFragment fragment = new AboutUsFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void gotoEntry(){

        startActivity(new Intent(this, EvaluationActivity.class));
    }

    public void gotoPreview(String url){

        Intent intent = new Intent(this, ImagePreviewActivity.class);
        intent.putExtra(Constants.KEY_IMAGEPATH, url);
        _context.startActivity(intent);
    }

    public void gotoIncidentDescriptionActivity(){

        startActivity(new Intent(this, IncidentDescriptionActivity.class));
        finish();
        Constants._from = Constants.FROM_PATRONLIST;

    }

    public void gotoDisputDetailsFragment(IncidentEntity error){

        txv_title.setText("Incident Details");

        DisputeDetailsFragment fragment = new DisputeDetailsFragment(this, error);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frm_container, fragment).commit();
    }

    public void gotoScan(){

        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }

    public static boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {

                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void checkLocationPermission() {

        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE ,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.CAPTURE_VIDEO_OUTPUT};

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }

    }

    @Override
    public void onBackPressed() {
        if (ui_drawerlayout.isDrawerOpen(GravityCompat.START)){

            ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else onExit();
    }
}
