package com.club.security.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.club.security.model.IncidentEntity;

import java.util.ArrayList;

/**
 * Created by HugeRain on 4/28/2017.
 */

public class ErrorListViewAdapter extends BaseAdapter{

    MainActivity _activity;
    ArrayList<IncidentEntity> _allError = new ArrayList<>();


    public ErrorListViewAdapter(MainActivity activity, ArrayList<IncidentEntity> errors){

        _activity = activity;
        _allError = errors;
    }

    @Override
    public int getCount() {
        return _allError.size();
    }

    @Override
    public Object getItem(int position) {
        return _allError.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ErrorHolder holder;

        if (convertView == null){

            holder = new ErrorHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_error, parent, false);

            holder.lyt_error = (LinearLayout)convertView.findViewById(R.id.lyt_error);
            holder.imv_photo = (ImageView)convertView.findViewById(R.id.imv_photo);
            holder.txv_event = (TextView)convertView.findViewById(R.id.txv_event);
            holder.txv_date = (TextView)convertView.findViewById(R.id.txv_date);

            holder.lyt_error.setBackgroundResource(R.drawable.grey_stroke);
            convertView.setTag(holder);

        } else {

            holder = (ErrorHolder)convertView.getTag();
        }


        final IncidentEntity error = (IncidentEntity)_allError.get(position);

        if (error.get_incident_dispute_status() == 1)
            holder.lyt_error.setBackgroundResource(R.drawable.fill_yellow);
        else holder.lyt_error.setBackgroundResource(R.drawable.grey_stroke);

        Glide.with(_activity).load(error.get_incident_picture()).placeholder(R.drawable.bg_non_profile).into(holder.imv_photo);
        holder.txv_event.setText(error.get_incident_description());
        holder.txv_date.setText(error.get_incident_date());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.gotoDisputDetailsFragment(error);
            }
        });

        return convertView;
    }

    public class ErrorHolder{

        LinearLayout lyt_error;
        ImageView imv_photo;
        TextView txv_event;
        TextView txv_date;

    }
}
