package com.club.security.model;

import java.io.Serializable;

/**
 * Created by HugeRain on 4/28/2017.
 */

public class IncidentEntity implements Serializable {

    int _id = 0;
    int _incident_dispute_status = 0; // 0: not submit, 1 : already submit:
    String _incident_description = "";
    String _incident_picture = "";
    String _incident_date = "";
    String _incident_type = "";


    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int get_incident_dispute_status() {
        return _incident_dispute_status;
    }

    public void set_incident_dispute_status(int _incident_dispute_status) {
        this._incident_dispute_status = _incident_dispute_status;
    }

    public String get_incident_description() {
        return _incident_description;
    }

    public void set_incident_description(String _incident_description) {
        this._incident_description = _incident_description;
    }

    public String get_incident_picture() {
        return _incident_picture;
    }

    public void set_incident_picture(String _incident_picture) {
        this._incident_picture = _incident_picture;
    }

    public String get_incident_date() {
        return _incident_date;
    }

    public void set_incident_date(String _incident_date) {
        this._incident_date = _incident_date;
    }

    public String get_incident_type() {
        return _incident_type;
    }

    public void set_incident_type(String _incident_type) {
        this._incident_type = _incident_type;
    }

}
