package com.club.security.commons;

import com.club.security.model.IncidentEntity;
import com.club.security.model.PatronEntity;

import java.util.ArrayList;

/**
 * Created by HGS on 12/11/2015.
 */
public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;
    public static final int SPLASH_TIME = 2000;
    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final int RECENT_MESSAGE_COUNT = 20;


    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;

    public static final String USER = "user";
    public static final String CLASS = "class";
    public static final String KEY_ROOM = "room";

    public static final String XMPP_START = "xmpp";
    public static final int XMPP_FROMBROADCAST = 0;
    public static final int XMPP_FROMLOGIN = 1;

    public static final String KEY_SEPERATOR = "#";
    public static final String KEY_ROOM_MARKER = "ROOM#";

    public static final String KEY_LOGOUT = "logout";

    public static final int NORMAL_NOTI_ID = 1;

    public static final String KEY_IMAGEPATH = "image_path";
    public static final String KEY_POSITION = "position";

    public static final int MEMBER = 1; // 0 : security : 1: member:

    public static int SECURITY = 0;

    public static ArrayList<IncidentEntity> ERRORS = new ArrayList<>();

    public static int CURRENT_PAGE = 0; // 1: from landing page 2: report incident page 3: dispute incident
    public static int CREATE = 0; // 3: with scanner 5: create

    public static String DESCRIPTION = "";
    public static String KEEP_DESCRIPTION = "";

    public static final String KEY_IIN = "iin";
    public static final String KEY_DAQ = "daq";
    public static final String KEY_DBA = "dba";
    public static final String KEY_DBB = "dbb";

    public static final String KEY_INCIDENT_TYPE = "incident_type";
    public static final String KEY_DESCRIPTION = "incident_description";
    public static final String KEY_PATRON_ID = "patron_id";
    public static final String KEY_PHOTO_URL = "photo_url";

    public static int _from = 0;
    public static final int FROM_SUBMISSION = 111;
    public static final int FROM_SCAN = 222;
    public static final int FROM_PATRONLIST = 333;
    public static final int FROM_SCAN_DISPUTE = 444;

    public static  int selectedSpinnerIndex = 0;


}
