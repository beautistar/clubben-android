package com.club.security.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.club.security.R;
import com.club.security.adapter.ImagePreviewAdapter;
import com.club.security.base.CommonActivity;
import com.club.security.commons.Constants;
import com.club.security.utils.TouchImageView;

import java.util.ArrayList;

public class ImagePreviewActivity extends CommonActivity implements View.OnClickListener {

    ViewPager ui_viewPager;
    ImagePreviewAdapter _adapter;
    //TextView ui_txvImageNo;

    //ArrayList<String> _imagePaths = new ArrayList<>();
    String _imagePaths = "";
    int _position = 0;
    String _comment = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        _imagePaths = getIntent().getStringExtra(Constants.KEY_IMAGEPATH);
        //_position = getIntent().getIntExtra(Constants.KEY_POSITION,0);

        loadLayout();
    }

    private void loadLayout(){

        //ui_txvImageNo = (TextView)findViewById(R.id.txv_timeline_no);


        ImageView imvBack = (ImageView)findViewById(R.id.imv_back);
        imvBack.setOnClickListener(this);

        final TouchImageView imageView = (TouchImageView) findViewById(R.id.imv_photo);

        //Glide.with(this).load(_imagePaths).placeholder(R.drawable.bg_non_profile).into(imageView);

        Glide.with(_context).load(_imagePaths).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                imageView.setImageBitmap(resource);
            }
        });
   /*     ui_viewPager = (ViewPager)findViewById(R.id.viewpager);
        _adapter = new ImagePreviewAdapter(this);
        ui_viewPager.setAdapter(_adapter);
        _adapter.setDatas(_imagePaths);*/

        //ui_viewPager.setCurrentItem(_position);

        //ui_txvImageNo.setText((_position + 1) + " / " + _imagePaths.size());

        /*ui_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ui_txvImageNo.setText((position + 1) + " / " + _imagePaths.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;
        }
    }

}
