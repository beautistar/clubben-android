package com.club.security.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.club.security.ClubApplication;
import com.club.security.R;
import com.club.security.base.CommonActivity;
import com.club.security.commons.Commons;
import com.club.security.commons.Constants;
import com.club.security.commons.ReqConst;
import com.club.security.model.UserEntity;
import com.club.security.preference.PrefConst;
import com.club.security.preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.lang.String.valueOf;

public class LoginActivity extends CommonActivity implements View.OnClickListener {

    TextView txv_login;
    EditText edt_username, edt_password;
    boolean _isFromLogout = false ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initValue();
        loadLayout();
    }

    private void initValue() {

        Intent intent = getIntent();

        try {
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);

        } catch (Exception e){
        }
    }

    private void loadLayout() {

        txv_login = (TextView)findViewById(R.id.txv_login);
        txv_login.setOnClickListener(this);

        edt_username = (EditText)findViewById(R.id.edt_username);
        edt_password = (EditText)findViewById(R.id.edt_password);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_login);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_username.getWindowToken(), 0);

                return false;
            }
        });

        if (_isFromLogout){

            //   save user to empty
            Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME,"");
            Preference.getInstance().put(this,PrefConst.PREFKEY_USERPWD, "");

            edt_username.setText("");
            edt_password.setText("");

        } else {

            String _name = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERNAME, "");
            String _password = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD, "");

            edt_username.setText(_name);
            edt_password.setText(_password);

//            if ( _name.length()>0 && _password.length() > 0 ) {
//
//                progressLogin();
//            }
        }

    }

    public boolean checkValue(){

        if (edt_username.getText().toString().length() ==0){

            showAlertDialog("Please input user name");
            return false;

        } else if (edt_password.getText().toString().length() == 0){

            showAlertDialog("Please input password");
            return false;
        }

        return true;
    }

    private void progressLogin(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        try {

            String username = edt_username.getText().toString().trim().replace(" ","%20");
            username = URLEncoder.encode(username,"utf-8");

            String password = edt_password.getText().toString().trim().replace(" ","%20");
            password = URLEncoder.encode(password,"utf-8");

            String params = String.format("/%s/%s", username, password );
            url += params ;

            Log.d("loginURL==********==>", url);

        } catch (UnsupportedEncodingException e) {

            closeProgress();
            e.printStackTrace();
        }

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseLoginResponse(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ClubApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseLoginResponse(String json) {

        closeProgress();
        Log.d("LoginRespon==>", json);

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                UserEntity user = new UserEntity();
                JSONObject user_info = response.getJSONObject(ReqConst.RES_USER_INFO);

                user.set_idx(user_info.getInt(ReqConst.RES_USER_ID));
                user.set_user_name(user_info.getString(ReqConst.RES_USERNAME));
                user.set_email(user_info.getString(ReqConst.RES_EMAIL));
                user.set_permission(user_info.getInt(ReqConst.RES_PERMISSION));
                user.set_club_id(user_info.getInt(ReqConst.RES_CLUBID));
                user.set_club_name(user_info.getString(ReqConst.RES_CLUBNAME));

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERNAME, edt_username.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERPWD, edt_password.getText().toString().trim());

                Commons.g_user = user;

                showToast("Login Success");

                gotoMain();

            } else if (result_code == ReqConst.CODE_UNREGISTER_EMAIL){

                closeProgress();
                showAlertDialog("Unregistered Email");

            } else if (result_code == ReqConst.CODE_WRONG_PASSWORD){

                closeProgress();
                showAlertDialog("Wrong password \n Please try again..." );
            }

        } catch (JSONException e) {
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

    public void gotoMain(){

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_login:
                if (checkValue())
                    progressLogin();

           /*     if (checkValue())
                {
                    if (edt_username.getText().toString().equals("admission@test.com")){

                        Constants.SECURITY = 1;

                    } else {

                        Constants.SECURITY = 0;
                    }

                    Log.d("edt+++++++>", edt_username.getText().toString());

                    gotoMain();
                }
*/
                break;
        }
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
