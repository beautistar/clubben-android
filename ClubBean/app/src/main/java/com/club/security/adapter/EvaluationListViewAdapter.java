package com.club.security.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.club.security.R;
import com.club.security.activity.EvaluationActivity;
import com.club.security.commons.Commons;
import com.club.security.model.IncidentEntity;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

/**
 * Created by HugeRain on 5/5/2017.
 */

public class EvaluationListViewAdapter extends BaseAdapter {


    EvaluationActivity _activity;
    ArrayList<IncidentEntity> _errors = new ArrayList<>();

    public EvaluationListViewAdapter (EvaluationActivity activity, ArrayList<IncidentEntity> errors){

        this._activity = activity;
        this._errors = errors;

    }

    @Override
    public int getCount() {
        return _errors.size();
    }

    @Override
    public Object getItem(int position) {
        return _errors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Holder holder;

        if (convertView == null){

            holder = new Holder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate( R.layout.item_lst_evauation, parent, false);

            holder.imv_error = (RoundedImageView) convertView.findViewById(R.id.imv_event_photo);
            holder.txv_description = (TextView)convertView.findViewById(R.id.txv_description);
            holder.txv_date = (TextView)convertView.findViewById(R.id.txv_date);

            convertView.setTag(holder);

        } else {

            holder = (Holder)convertView.getTag();
        }

        final IncidentEntity error = _errors.get(position);

        Glide.with(_activity).load(error.get_incident_picture()).placeholder(R.drawable.bg_non_profile).into(holder.imv_error);
        holder.txv_description.setText(error.get_incident_description());
        holder.txv_date.setText(error.get_incident_date());

        holder.imv_error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.gotoPreview(error.get_incident_picture());
            }
        });

        return convertView;
    }

    public class Holder{

        RoundedImageView imv_error;
        TextView txv_description,txv_date;
    }
}
