package com.club.security.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.club.security.R;
import com.club.security.activity.MainActivity;
import com.club.security.fragment.PatronsPresentFragment;
import com.club.security.model.PatronEntity;

import java.util.ArrayList;

/**
 * Created by HugeRain on 4/29/2017.
 */

public class SelectedGridViewAdapter extends BaseAdapter {


    MainActivity _activity;
    ArrayList<PatronEntity> _available_users = new ArrayList<>();

    ArrayList<String> _user_photo_urls = new ArrayList<>();

    PatronsPresentFragment patronsPresentFragment =null;

    public SelectedGridViewAdapter(MainActivity activity, ArrayList<PatronEntity> users , PatronsPresentFragment patronsPresentFragment){

        _activity = activity;
        _available_users = users;
        this.patronsPresentFragment = patronsPresentFragment;
    }

    @Override
    public int getCount() {
        return _available_users.size();
    }

    @Override
    public Object getItem(int position) {
        return _available_users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final AvailableHolder holder;

        if (convertView == null){

            holder = new AvailableHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_grid_user, parent, false);

            holder.imv_user = (ImageView)convertView.findViewById(R.id.imv_image);
            holder.imv_checked = (ImageView)convertView.findViewById(R.id.imv_checked);

            convertView.setTag(holder);

        } else {

            holder = (AvailableHolder)convertView.getTag();
        }

        final PatronEntity available_user = _available_users.get(position);

        Glide.with(_activity).load(available_user.get_photo_url()).placeholder(R.drawable.bg_non_profile).into(holder.imv_user);


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.imv_checked.isSelected()){

                    holder.imv_checked.setSelected(!holder.imv_checked.isSelected());

                    _user_photo_urls.remove(available_user.get_photo_url());
                    int size = _user_photo_urls.size();

                    if (size == 0){

                        patronsPresentFragment.setSelectedUser("");
                    } else {
                        patronsPresentFragment.setSelectedUser(_user_photo_urls.get(size - 1));
                    }

                    patronsPresentFragment.removeItem(available_user);

                    notifyDataSetChanged();

                } else {

                    holder.imv_checked.setSelected(!holder.imv_checked.isSelected());
                    _user_photo_urls.add(available_user.get_photo_url());
                    int size = _user_photo_urls.size();
                    patronsPresentFragment.setSelectedUser(_user_photo_urls.get(size - 1));

                    patronsPresentFragment.addItem(available_user);

                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }

    public class AvailableHolder{

        ImageView imv_user;
        ImageView imv_checked;
    }
}
