package com.club.security.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.club.security.R;
import com.club.security.commons.Commons;
import com.club.security.commons.Constants;
import com.club.security.model.IncidentEntity;

public class IntroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);


        gotoLogin();

    }

    private void gotoLogin() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(IntroActivity.this, LoginActivity.class));
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}
