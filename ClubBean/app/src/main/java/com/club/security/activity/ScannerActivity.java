package com.club.security.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.club.security.R;
import com.club.security.base.CommonActivity;
import com.club.security.commons.Constants;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.pdf417.encoder.PDF417;
import com.microblink.activity.Pdf417ScanActivity;
import com.microblink.recognizers.BaseRecognitionResult;
import com.microblink.recognizers.RecognitionResults;
import com.microblink.recognizers.blinkbarcode.BarcodeType;
import com.microblink.recognizers.blinkbarcode.bardecoder.BarDecoderRecognizerSettings;
import com.microblink.recognizers.blinkbarcode.bardecoder.BarDecoderScanResult;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417RecognizerSettings;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417ScanResult;
import com.microblink.recognizers.blinkbarcode.simnumber.SimNumberRecognizerSettings;
import com.microblink.recognizers.blinkbarcode.zxing.ZXingRecognizerSettings;
import com.microblink.recognizers.blinkbarcode.zxing.ZXingScanResult;
import com.microblink.recognizers.settings.RecognitionSettings;
import com.microblink.recognizers.settings.RecognizerSettings;
import com.microblink.results.barcode.BarcodeDetailedData;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerActivity extends CommonActivity /*implements ZXingScannerView.ResultHandler*/ {

    private ZXingScannerView mScannerView;
    private TextView txv_barcode;
    private LinearLayout llScan;
    RadioGroup radioGroup1;
    String _iin ="", _daq = "", _dba = "", _dbb = "";

    //PDF417
    private static final String LICENSE_KEY = "XWE6QA2O-NWZY43UA-JVCN5VOM-FZY56OXS-3Q454LL5-4P7EHZVR-DP2SVMI2-ICVIRLQO";
    //private static final String LICENSE_KEY = "X6JY35K3-YCFCKW4G-67X6YOTS-LSZA3PMV-CIN7KKQR-LETMGNZZ-3YWX3Y76-IPTOBETO";
    private static final int MY_REQUEST_CODE = 1337;

    private static final String TAG = "Pdf417MobiDemo";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        loadLayout();
    }

    private void loadLayout() {

       /* mScannerView = new ZXingScannerView(this);

        //mScannerView.setFlash(true);
        mScannerView.setAutoFocus(true);
        llScan = (LinearLayout) findViewById(R.id.llScan);
        llScan.addView(mScannerView);*/

        ImageView imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoMain();
            }
        });

   /*     radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);

        RadioButton button_off = (RadioButton)findViewById(R.id.radio_new_connection);
        button_off.setChecked(true);
        // Checked change Listener for RadioGroup 1
        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch (checkedId)
                {
                    case R.id.radio_invitation:
                        mScannerView.setFlash(true);
                        break;
                    case R.id.radio_new_connection:
                        mScannerView.setFlash(false);
                        break;
                    default:
                        break;
                }
            }
        });*/

        scanPdf417();

    }

 /*   @Override
    protected void onDestroy() {
        super.onDestroy();
        mScannerView.setFlash(false);
    }*/

    public void gotoMain(){

        startActivity(new Intent(ScannerActivity.this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

/*    @Override
    public void handleResult(Result result) {

        Map<ResultMetadataType,Object> resultMetadata=result.getResultMetadata();

        mScannerView.stopCamera();


        if (((getIIN(result.getText())).equals("N/A") || (getDAQ(result.getText())).equals("N/A") || (getDBA(result.getText())).equals("N/A") || (getDBB(result.getText())).equals("N/A"))){

            showAlertDialog2("Invalid number");

        } else showAlertDialog1(result.getText());

    }*/

    public void showAlertDialog2(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.try_again),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        gotoMain();
                    }
                });
        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    public void showAlertDialog1(String msg) {

        final AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle("List of scan results");

        String message = "IIN : " + getIIN(msg) + "\n\n"
                       + "DAQ : " + getDAQ(msg) + "\n\n"
                       + "DBA : " + getDBA(msg) + "\n\n"
                       + "DBB : " + getDBB(msg) ;

        alertDialog.setMessage(message);

        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.submit),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        Log.d("Current_page+>", String.valueOf(Constants.CURRENT_PAGE));

                        if (Constants.CURRENT_PAGE == 1){

                            //_activity.gotoIndentFragment();

                            Intent intent = new Intent(ScannerActivity.this, EvaluationActivity.class);

                            intent.putExtra(Constants.KEY_IIN, _iin);
                            intent.putExtra(Constants.KEY_DAQ, _daq);
                            intent.putExtra(Constants.KEY_DBA, _dba);
                            intent.putExtra(Constants.KEY_DBB, _dbb);

                            Log.d("IIN===>", _iin);
                            Log.d("DAQ===>", _daq);
                            Log.d("DBA===>", _dba);

                            startActivity(intent);
                            overridePendingTransition(0,0);
                            finish();

                        } else if (Constants.CURRENT_PAGE == 2){

                            Intent intent = new Intent(ScannerActivity.this, IncidentDescriptionActivity.class);

                            Constants._from = Constants.FROM_SCAN;

                            intent.putExtra(Constants.KEY_IIN,_iin);
                            intent.putExtra(Constants.KEY_DAQ, _daq);
                            intent.putExtra(Constants.KEY_DBA, _dba);
                            intent.putExtra(Constants.KEY_DBB, _dbb);

                            startActivity(intent);
                            overridePendingTransition(0,0);
                            finish();

                        } else if (Constants.CURRENT_PAGE == 3){

                            Intent intent = new Intent(ScannerActivity.this, MainActivity.class);

                            intent.putExtra(Constants.KEY_IIN,_iin);
                            intent.putExtra(Constants.KEY_DAQ, _daq);
                            intent.putExtra(Constants.KEY_DBA, _dba);
                            intent.putExtra(Constants.KEY_DBB, _dbb);

                            startActivity(intent);
                            overridePendingTransition(0,0);
                            finish();

                        } else {

                            return;
                        }

                        finish();

                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,  _context.getString(R.string.cancel),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        alertDialog.dismiss();
                        gotoMain();
                    }
                });

        alertDialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();

      /*  // Register ourselves as a handler for scan results.
        mScannerView.setResultHandler(this);

        // Start camera on resume
        mScannerView.startCamera();*/
    }

/*
    @Override
    public void onPause() {
        super.onPause();

        // Stop camera on pause
        mScannerView.stopCamera();
    }
*/


    // get Issuer Identification Number
    private String getIIN(String str) {

        try {

            Pattern pattern = Pattern.compile("(\\d{6})");

            //   \d is for a digit
            //   {} is the number of digits here 6.

            Matcher matcher = pattern.matcher(str);

            if (matcher.find()) {
                _iin = matcher.group(1);  // 6 digit number
                Log.d("6 IIN : ", _iin);
            }

        } catch (Exception e) {

            e.printStackTrace();
//            showAlertDialog("Invalid ID");
            return "N/A";
        }

        return _iin;
    }

    // get DAQ : License Number
    private String getDAQ(String str) {


        try {

            String splitedStr = str.split("DAQ")[1]; // cut "DAQ" before string
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(splitedStr);

            while (m.find()) {
                _daq = m.group();
                break;
            }


        } catch (Exception e) {

            e.printStackTrace();
//            showAlertDialog("Invalid ID");
            return "N/A";
        }

        return _daq;
    }

    // get DBA  from scanned string : Expiration Date
    private String getDBA(String str) {

        try {

            String splitedStr = str.split("DBA")[1];

            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(splitedStr);

            while (m.find()) {
                _dba = m.group();
                Log.d("DBA : ", m.group());
                break;
            }
        } catch (Exception e) {

            return "N/A";
//            showAlertDialog("Invalid ID");
        }

        return _dba;
    }

    // get DBB DBB: Date of Birth
    private String getDBB(String str) {


        try {

            String splitedStr = str.split("DBB")[1];

            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(splitedStr);

            while (m.find()) {
                _dbb = m.group();
                Log.d("DBB : ", m.group());
                break;
            }

        } catch (Exception e) {

            return "N/A";
//            showAlertDialog("Invalid ID");
        }

        return _dbb;
    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    public void scanPdf417() {

        com.microblink.util.Log.i(TAG, "scan will be performed");
        // Intent for ScanActivity
        Intent intent = new Intent(this, Pdf417ScanActivity.class);
        intent.putExtra(Pdf417ScanActivity.EXTRAS_LICENSE_KEY, LICENSE_KEY);


        Pdf417RecognizerSettings pdf417RecognizerSettings = new Pdf417RecognizerSettings();
        pdf417RecognizerSettings.setNullQuietZoneAllowed(true);
        BarDecoderRecognizerSettings oneDimensionalRecognizerSettings = new BarDecoderRecognizerSettings();
        oneDimensionalRecognizerSettings.setScanCode39(true);
        oneDimensionalRecognizerSettings.setScanCode128(true);
        ZXingRecognizerSettings zXingRecognizerSettings = new ZXingRecognizerSettings();
        zXingRecognizerSettings.setScanQRCode(true);
        zXingRecognizerSettings.setScanITFCode(true);

        SimNumberRecognizerSettings sm = new SimNumberRecognizerSettings();

        RecognitionSettings recognitionSettings = new RecognitionSettings();

        recognitionSettings.setRecognizerSettingsArray(new RecognizerSettings[]{pdf417RecognizerSettings, oneDimensionalRecognizerSettings, zXingRecognizerSettings,sm});

        intent.putExtra(Pdf417ScanActivity.EXTRAS_RECOGNITION_SETTINGS, recognitionSettings);

        intent.putExtra(Pdf417ScanActivity.EXTRAS_SHOW_DIALOG_AFTER_SCAN, false);

        intent.putExtra(Pdf417ScanActivity.EXTRAS_SPLASH_SCREEN_LAYOUT_RESOURCE ,R.layout.splash_blank);

        intent.putExtra(Pdf417ScanActivity.EXTRAS_ALLOW_PINCH_TO_ZOOM, false);

        intent.putExtra(Pdf417ScanActivity.EXTRAS_SHOW_FOCUS_RECTANGLE, false);

        intent.putExtra(Pdf417ScanActivity.EXTRAS_SPLASH_SCREEN_LAYOUT_RESOURCE, true);

        startActivityForResult(intent, MY_REQUEST_CODE);
    }

    private boolean checkIfDataIsUrlAndCreateIntent(String data) {

        // if barcode contains URL, create intent for browser
        // else, contain intent for message
        boolean barcodeDataIsUrl;
        try {
            @SuppressWarnings("unused")
            URL url = new URL(data);
            barcodeDataIsUrl = true;
        } catch (MalformedURLException exc) {
            barcodeDataIsUrl = false;
        }

        if (barcodeDataIsUrl) {
            // create intent for browser
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(data));
            startActivity(Intent.createChooser(intent, getString(R.string.UseWith)));
        }
        return barcodeDataIsUrl;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MY_REQUEST_CODE && resultCode == Pdf417ScanActivity.RESULT_OK) {

            Log.d("aaaaaaaaaaaaaaaaaa====>", "aaaaaaaaaaaaaa");

            RecognitionResults results = data.getParcelableExtra(Pdf417ScanActivity.EXTRAS_RECOGNITION_RESULTS);
            // Get scan results array. If scan was successful, array will contain at least one element.
            // Multiple element may be in array if multiple scan results from single image were allowed in settings.
            BaseRecognitionResult[] resultArray = results.getRecognitionResults();

            Log.d("===resultArray==>", String.valueOf(resultArray));

            // Each recognition result corresponds to active recognizer. As stated earlier, there are 3 types of
            // recognizers available (PDF417, Bardecoder and ZXing), so there are 3 types of results
            // available.

            StringBuilder sb = new StringBuilder();

            for(BaseRecognitionResult res : resultArray) {
                if(res instanceof Pdf417ScanResult) { // check if scan result is result of Pdf417 recognizer
                    Pdf417ScanResult result = (Pdf417ScanResult) res;
                    // getStringData getter will return the string version of barcode contents
                    String barcodeData = result.getStringData();

                    Log.d("String++Result==>", barcodeData);
                    // isUncertain getter will tell you if scanned barcode contains some uncertainties
                    boolean uncertainData = result.isUncertain();
                    // getRawData getter will return the raw data information object of barcode contents
                    BarcodeDetailedData rawData = result.getRawData();
                    // BarcodeDetailedData contains information about barcode's binary layout, if you
                    // are only interested in raw bytes, you can obtain them with getAllData getter
                    byte[] rawDataBuffer = rawData.getAllData();

                    // if data is URL, open the browser and stop processing result
                    if(checkIfDataIsUrlAndCreateIntent(barcodeData)) {
                        return;
                    } else {
                        // add data to string builder
                        sb.append("PDF417 scan data");
                        if (uncertainData) {
                            sb.append("This scan data is uncertain!\n\n");
                        }
                        sb.append(" string data:\n");
                        sb.append(barcodeData);
                        if (rawData != null) {
                            sb.append("\n\n");
                            sb.append("PDF417 raw data:\n");
                            sb.append(rawData.toString());
                            sb.append("\n");
                            sb.append("PDF417 raw data merged:\n");
                            sb.append("{");
                            for (int i = 0; i < rawDataBuffer.length; ++i) {
                                sb.append((int) rawDataBuffer[i] & 0x0FF);
                                if (i != rawDataBuffer.length - 1) {
                                    sb.append(", ");
                                }
                            }
                            sb.append("}\n\n\n");
                        }
                    }
                } else if(res instanceof BarDecoderScanResult) { // check if scan result is result of BarDecoder recognizer
                    BarDecoderScanResult result = (BarDecoderScanResult) res;
                    // with getBarcodeType you can obtain barcode type enum that tells you the type of decoded barcode
                    BarcodeType type = result.getBarcodeType();
                    // as with PDF417, getStringData will return the string contents of barcode
                    String barcodeData = result.getStringData();
                    if(checkIfDataIsUrlAndCreateIntent(barcodeData)) {
                        return;
                    } else {
                        sb.append(type.name());
                        sb.append(" string data:\n");
                        sb.append(barcodeData);
                        sb.append("\n\n\n");
                    }
                } else if(res instanceof ZXingScanResult) { // check if scan result is result of ZXing recognizer
                    ZXingScanResult result= (ZXingScanResult) res;
                    // with getBarcodeType you can obtain barcode type enum that tells you the type of decoded barcode
                    BarcodeType type = result.getBarcodeType();
                    // as with PDF417, getStringData will return the string contents of barcode
                    String barcodeData = result.getStringData();

                    Log.d("Sub_BarCode_Data==>", barcodeData);
                    if(checkIfDataIsUrlAndCreateIntent(barcodeData)) {
                        return;
                    } else {
                        sb.append(type.name());
                        sb.append(" string data:\n");
                        sb.append(barcodeData);
                        sb.append("\n\n\n");
                    }
                }
            }

            Log.d("SUB_STING===?>", sb.toString());

           /* Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, sb.toString());
            startActivity(Intent.createChooser(intent, getString(R.string.UseWith)));*/

            if (((getIIN(sb.toString())).equals("N/A") || (getDAQ(sb.toString())).equals("N/A") || (getDBA(sb.toString())).equals("N/A") || (getDBB(sb.toString())).equals("N/A"))){

                showAlertDialog2("Invalid number");


            } else {
                showAlertDialog1(sb.toString());
            }
        }
    }


}
